

function muestra_oculta(id) {
    if (document.getElementById) { //Se obtiene el id
        var el = document.getElementById(id); //Se define la variable "el" igual a nuestro div
        el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //Damos un atributo display:none que oculta el div


        /* Determinaremos el InnerJoin en el caso de encontrase en estado Block o None */



        if (id == 'bookday') {
            if (el.style.display == 'none') {
                document.getElementById("texto_bookday").innerHTML = "BookDay +";
                document.getElementById("bookday_").value = "";
            } else {
                document.getElementById("texto_bookday").innerHTML = "BookDay -";
            }
        }



        if (id == 'checkin') {
            if (el.style.display == 'none') {
                document.getElementById("texto_checkin").innerHTML = "CheckIn +";
                document.getElementById("desde").value = "";
            } else {
                document.getElementById("texto_checkin").innerHTML = "CheckIn -";
            }
        }



        if (id == 'checkout') {
            if (el.style.display == 'none') {
                document.getElementById("texto_checkout").innerHTML = "CheckOut +";
                document.getElementById("hasta").value = "";
            } else {
                document.getElementById("texto_checkout").innerHTML = "CheckOut -";
            }
        }



        if (id == 'companie') {
            if (el.style.display == 'none') {
                document.getElementById("texto_companie").innerHTML = "Travel Agent +";
                document.getElementById("companie_").value = "ALL";
            } else {
                document.getElementById("texto_companie").innerHTML = "Travel Agent -";
            }
        }

        if (id == 'regimen') {
            if (el.style.display == 'none') {
                document.getElementById("texto_regimen").innerHTML = "Regimen +";
                document.getElementById("regimen_").value = "ALL";
            } else {
                document.getElementById("texto_regimen").innerHTML = "Regimen -";
            }
        }

        if (id == 'apartamento') {
            if (el.style.display == 'none') {
                document.getElementById("texto_apartamento").innerHTML = "Apartament +";
                document.getElementById("apartamento_").value = "ALL";
            } else {
                document.getElementById("texto_apartamento").innerHTML = "Apartament -";
            }
        }

        if (id == 'pago') {
            if (el.style.display == 'none') {
                document.getElementById("texto_pago").innerHTML = "Payment +";
                document.getElementById("pago_").value = "ALL";
            } else {
                document.getElementById("texto_pago").innerHTML = "Payment -";
            }
        }


        if (id == 'estado') {
            if (el.style.display == 'none') {
                document.getElementById("texto_estado").innerHTML = "Status +";
                document.getElementById("estado_").value = "ALL";
            } else {
                document.getElementById("texto_estado").innerHTML = "Status -";
            }
        }






    }

}

window.onload = function() {

    /*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/

    muestra_oculta('bookday'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('checkin'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('checkout'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('companie'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('regimen'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('apartamento'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('pago'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */
    muestra_oculta('estado'); /* "contenido_a_mostrar" es el nombre que le dimos al DIV */


}





function limpiarFormulario() { // Limpia el Formulario, reseteando los Value
    document.getElementById("miForm").reset();
}