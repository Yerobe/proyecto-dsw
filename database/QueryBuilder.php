<?php



abstract class QueryBuilder

{

  private $connection; // Conexión a establecer
  private $table; // Tabla a ser usada
  private $classEntity; // Clase a ser usada

  public function __construct(string $table, string $classEntity)
  {

    $this->connection = App::getConnection(); // Conexión a la BBDD

    $this->table = $table; // Tabla de la BBDD

    $this->classEntity = $classEntity; // Modelo

  }



  public function executeQuery(string $sql): array // Me permite centralizar a ejecución de los SCRIPT y generar el exeption

  {

    $pdoStatement = $this->connection->prepare($sql);


    if ($pdoStatement->execute() === false) {
      throw new QueryException("No se ha podido ejecutar la consulta");
    }



    return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
  }


  /* ================= FUNCIÓN DE LOGIN ================= */

  public function findUser(string $nombre, string $password)
  {

    $sql = "SELECT * FROM usuarios WHERE Nombre = '$nombre' AND Password = sha1('$password') AND Estado = 1 "; // Comprobamos la existencia del Usiario



    $pdoStatement = $this->executeQuery($sql); // Ejecucución de esta

    if (empty($pdoStatement)) { // En el caso de que no encontremos Usuarios, lo buscaremos pero comprobaremos si esta Deshabilitado
      $sql_disabled = "SELECT * FROM usuarios WHERE Nombre = '$nombre' AND Password = sha1('$password') AND Estado = 0 "; // Estado = 0 [Deshabilitado]
      $pdoStatement_disabled = $this->executeQuery($sql_disabled); // Ejecución SQL

      if (empty($pdoStatement_disabled)) { // No se ha encontrado Usuario en ninguno de los Estados [Habilitado - Deshabilitado]
        throw new NotFoundException("Usuario o Contraseña Incorrecta");
      } else {
        throw new NotFoundException("Usuario deshabilitado");
      }
    }

    return $pdoStatement;
  }



  /* =========== HOME =========== */

  public function finReservationCheckInforDay(string $fecha)
  {

    $sql = "SELECT * FROM reservas WHERE Fecha_Entrada = '$fecha' AND Estado = 1 "; // Reservas que la fecha de entrada, sea el día seleccionado

    $pdoStatement = $this->executeQuery($sql); // Ejecucución de esta


    return $pdoStatement;
  }


  public function finReservationCheckOutforDay(string $fecha)
  {

    $sql = "SELECT * FROM reservas WHERE Fecha_Salida = '$fecha' AND Estado = 1 ";

    $pdoStatement = $this->executeQuery($sql);


    return $pdoStatement;
  }






  public function findAll() /* Buscar Todo */
  {
    $sql = "SELECT * FROM $this->table"; // Muestra todos los registros de la tabla indicada


    $pdoStatement = $this->executeQuery($sql);

    if (empty($pdoStatement))
      throw new NotFoundException("No se ha encontrado registros");
    return $pdoStatement;
  }



  public function findAllwithEstatus() // Buscamos todas las Reservas que no esten Canceladas
  {

    $sql = "SELECT * FROM $this->table WHERE Estado = 1";


    $pdoStatement = $this->executeQuery($sql);

    if (empty($pdoStatement))
      throw new NotFoundException("No se ha encontrado registros en algunos de los campos");
    return $pdoStatement;
  }




  /* =========== RESERVAS =========== */

  public function findReservationWithFilter(string $filtro) // Ejecutamos un script pasado por variable de forma completa
  {
    $sql = "$filtro";

    $pdoStatement = $this->executeQuery($sql);

    if (empty($pdoStatement))
      throw new NotFoundException("No se ha encontrado registros en algunos de los campos");
    return $pdoStatement;
  }



  /* =========== USUARIOS =========== */

  public function findUserbyName(string $nombre) // Obtenemos los datos del usuario por su nombre
  {

    $sql = "SELECT * FROM $this->table WHERE Nombre = '$nombre' ";


    $pdoStatement = $this->executeQuery($sql);

    if (empty($pdoStatement))
      throw new NotFoundException("No se ha encontrado el Usuario");
    return $pdoStatement;
  }



  public function findUserByNIF($NIF) // Obtenemos los datos del usuario por su DNI
  {

    $sql = "SELECT * FROM $this->table WHERE NIF = '$NIF' ";

    $pdoStatement = $this->executeQuery($sql);

    return $pdoStatement;
  }






  public function save_reserva(IEntity $entity): void // Realizamos un INSERT de las reservas
  {

    try {

      $parameters = $entity->toArray();
      $sql = sprintf(
        "insert into %s (%s) values (%s)", // Nos devuelve un String Formateado
        $this->table,

        implode(", ", array_keys($parameters)), // Unimos las claves del array en un mismo string

        ":" . implode(", :", array_keys($parameters))

      );


      $statement = $this->connection->prepare($sql); // Introducimos en una variable la preparación de la línea SQL
      // ;
      $statement->execute($parameters); // Ejecutamos el SQL a la conexión realizada


    } catch (PDOException $exception) {

      throw new QueryException("Error al insertar en la BBDD.");
    }
  }




  public function update_reserva(IEntity $entity, int $id) // Actualizamos una Reserva determinada
  {



    $parameters = $entity->toArray();

    $numero_reserva = $parameters["Numero_Reserva"];
    $operador = $parameters["Operador"];
    $book_day = $parameters["Book_Day"];
    $fecha_entrada = $parameters["Fecha_Entrada"];
    $fecha_salida = $parameters["Fecha_Salida"];
    $pension = $parameters["Pension"];
    $nombre = $parameters["Nombre"];
    $apellidos = $parameters["Apellidos"];
    $numero_adultos = $parameters["Numero_Adultos"];
    $numero_ninios = $parameters["Numero_Ninios"];
    $pais = $parameters["Pais"];
    $mascota = $parameters["Mascota"];
    $prepago = $parameters["Prepago"];
    $precio_total = $parameters["Precio_Total"];
    $comentario = $parameters["Comentario"];
    $estado = $parameters["Estado"];
    $usuario_modifica = $parameters["Usuario_Modifica"];
    $nombre_archivo = $parameters["Nombre_Archivo"];

    if ($parameters["Telefono"] == "") {  // El caso en el que la Reserva no cuente con número de Teléfono
      $telefono = NULL;
    } else {
      $telefono = $parameters["Telefono"];
    }



    if ($parameters["Numero_Apartamento"] == "0") { // No podemos definir null como value, ya que es de tipo String, por lo que interpretamos 0 como Null y realizamos la conversión
      $numero_apartamento = "NULL";
    } else {
      $numero_apartamento = $parameters["Numero_Apartamento"];
    }


    $sql = "UPDATE $this->table SET Numero_Reserva = '$numero_reserva', Operador = $operador, Book_Day = '$book_day', 
    Fecha_Entrada = '$fecha_entrada' , Fecha_Salida = '$fecha_salida' , Pension = '$pension' , Nombre = '$nombre',
    Apellidos = '$apellidos' , Numero_Adultos = $numero_adultos, Numero_Ninios = $numero_ninios , Pais = $pais ,
    Mascota = $mascota , Prepago = '$prepago' , Precio_Total = '$precio_total' , Comentario = '$comentario' ,
    Estado = $estado , Telefono = '$telefono' , Numero_Apartamento = $numero_apartamento , Usuario_Modifica = '$usuario_modifica' , 
    Nombre_Archivo = '$nombre_archivo'
    WHERE Id = $id";


    $pdoStatement = $this->connection->prepare($sql);


    if ($pdoStatement->execute() === false) {
      throw new QueryException("No se ha podido ejecutar la consulta");
    }
  }




  /* ============= BOOKING =============  */


  public function finReservationBookDayToday() // Obtenemos las reservas creadas es día de HOY
  {

    $fecha = date('Y/m/d'); // Obtenemos el día de Hoy

    $sql = "SELECT * FROM reservas WHERE Fecha_Creado = '$fecha' AND Estado = 1 ";

    $pdoStatement = $this->executeQuery($sql);


    return $pdoStatement;
  }



  /* ============= RESTAURANTE =============  */


  public function findallWithDay($fecha)
  { // Busqueda de reservas por un día en concreto


    $sql = "SELECT * FROM reservas WHERE Estado = 1 AND Fecha_Entrada <= '$fecha' AND Fecha_Salida >= '$fecha';"; // Comprobamos la existencia de la Reserva

    $pdoStatement = $this->executeQuery($sql); // Ejecucución de esta


    return $pdoStatement;
  }





  /* ============= REPORTES =============  */


  public function findallWithWhere($where) // Me permite buscar todo en una table, pero con condicones
  {

    $sql = "SELECT * FROM $this->table $where";

    $pdoStatement = $this->executeQuery($sql);


    return $pdoStatement;
  }



  /* ============= TODAY ============= */

  public function findRoomsbyCategory($fecha, $tipo_apartamento)
  {

    $sql = "SELECT * FROM habitaciones WHERE N_Habitacion IN (SELECT Numero_Apartamento FROM reservas WHERE Estado = 1 AND Fecha_Entrada <= '$fecha' AND Fecha_Salida > '$fecha') AND Tipo = '$tipo_apartamento'";

    $pdoStatement = $this->executeQuery($sql); // Ejecucución de esta

    return $pdoStatement;
  }
}
