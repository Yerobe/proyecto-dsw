<?php



class Connection{ // Creamos la conexión


    public static function make() {

        try {

            $config = App::get("config")["database"];

            $connection = new PDO(

                $config["connection"] . ";dbname=" . $config["name"], 
               
                $config["username"],
               
                $config["password"], 
               
                $config["options"]);


               
        }
       
        catch (AppException $AppException) { // Error de conexión - Usuario - Contraseña - BD - Otros errores

            throw new AppException("No se ha podido conectar con la BBDD");
           
            }
       
        return $connection;
       
       }

}



?>