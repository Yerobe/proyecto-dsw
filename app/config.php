<?php

// Contiene la Información con la que generaremos el objeto Conexión

return [

    "database" => [
   
    "name" => "LaPiramide", // Nombre de la Base de Datos
   
    "username" => "Yerobe", // Usuario de la Base de Datos
   
    "password" => "12345", // Contraseña de la Base de Datos
   
    "connection" => "mysql:host=localhost",  // Host de Conexión
   
    "options" => [
   
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
   
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
   
    PDO::ATTR_PERSISTENT => true
   
    ]

    ]
   
   ];


?>