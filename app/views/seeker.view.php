<?php include __DIR__ . "../../views/partials/navbar.partial.php"; // Importación del Nav Lateral 
?>

<?php require __DIR__ . "/../views/partials/inicio-doc.part.php"; ?>

<script src="js/seeker.js"></script>
<script src="js/date-min.js"></script>


</head>


<body id="page-top" class="fondo">




    <?php require __DIR__ . "/../views/partials/topbar.partial.php"; ?>


    <div class="row alto">

        <?php navegacion_Lateral($pagina_actual = "seeker"); ?>


        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mt-5">


            <div class="row mt-5">
                <div class="col col-12">
                    <h3 style="font-size: 40px;" class="display-3 blockquote">Booking Search Engine</h3>
                    <hr class="border-top border-dark">
                </div>
            </div>


            <div class="col col-12 bg-white mt-5 p-3" id="Rooms_Border">
                <div class="row p-3">


                    <div class="col col-12 mt-3">


                        <div class="row mb-3 ">
                            <div class="col col-12">
                                <a id="texto_bookday" style='cursor: pointer;' onClick="muestra_oculta('bookday')" title="" class="rounded p-2 bg-dark text-white"></a>
                                <a id="texto_checkin" style='cursor: pointer;' onClick="muestra_oculta('checkin')" title="" class="rounded p-2 bg-dark text-white"></a>
                                <a id="texto_checkout" style='cursor: pointer;' onClick="muestra_oculta('checkout')" title="" class="rounded p-2 bg-dark text-white"></a>
                                <a id="texto_companie" style='cursor: pointer;' onClick="muestra_oculta('companie')" title="" class="rounded p-2 bg-dark text-white"></a>

                                <a id="texto_regimen" style='cursor: pointer;' onClick="muestra_oculta('regimen')" title="" class="rounded p-2 bg-dark text-white"></a>
                                <a id="texto_apartamento" style='cursor: pointer;' onClick="muestra_oculta('apartamento')" title="" class="rounded p-2 bg-dark text-white"></a>
                                <a id="texto_pago" style='cursor: pointer;' onClick="muestra_oculta('pago')" title="" class="rounded p-2 bg-dark text-white"></a>
                                <a id="texto_estado" style='cursor: pointer;' onClick="muestra_oculta('estado')" title="" class="rounded p-2 bg-dark text-white"></a>
                            </div>


                        </div>




                        <form style="background-color: #F8FBF8;" id="miForm" action="seeker?pagina=0" method="POST" class="border rounded border-dark">
                            <div class="form-group row p-3 ">
                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="numeroapartamento">Search</label>
                                    <input id="numeroreserva" class="form-control" id="numeroapartamento" type="text" name="numero_reserva" value="<?php echo @$numero; ?>"></p>
                                </div>


                                <div id="bookday" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="bookday">BookDay</label>
                                    <input id="bookday_" name="bookday" type="date" class="form-control" value="<?php echo @$bookday; ?>">
                                </div>


                                <div id="checkin" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="tipoapt">Check-In</label>
                                    <input onchange="myFunction()" id="desde" name="fecha_entrada" type="date" class="form-control" value="<?php echo @$checkin; ?>">
                                </div>

                                <div id="checkout" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="tipoapt">Check-Out</label>
                                    <input id="hasta" name="fecha_salida" type="date" class="form-control" value="<?php echo @$checkout; ?>">
                                </div>


                                <div id="companie" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">

                                    <label for="email">Travel Agent :</label>

                                    <select id="companie_" name="operador" id="inputState" class="form-control">

                                        <option value="ALL">Everyone</option>

                                        <?php foreach ($operadores as $operador) : ?>

                                            <option value="<?= $operador->getId() ?>"><?= $operador->getNombre(); ?></option>

                                        <?php endforeach; ?>
                                    </select>

                                </div>



                                <div id="regimen" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="email">Regime type :</label>

                                    <select id="regimen_" name="regimen" id="inputState" class="form-control">
                                        <option <?php if (isset($regimen) && $regimen == "ALL") {
                                                    echo "selected";
                                                } ?> value="ALL">Everyone</option>
                                        <option <?php if (isset($regimen) && $regimen == "Only Bed") {
                                                    echo "selected";
                                                } ?> value="Only Bed">Only Bed</option>
                                        <option <?php if (isset($regimen) && $regimen == "Breakfast") {
                                                    echo "selected";
                                                } ?> value="Breakfast">Breakfast</option>
                                        <option <?php if (isset($regimen) && $regimen == "Half Board") {
                                                    echo "selected";
                                                } ?> value="Half Board">Half Board</option>
                                        <option <?php if (isset($regimen) && $regimen == "Full Board") {
                                                    echo "selected";
                                                } ?> value="Full Board">Full Board</option>
                                    </select>
                                </div>



                                <div id="apartamento" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="email">Apartament :</label>



                                    <select id="apartamento_" name="apartamento" id="inputState" class="form-control">
                                        <option value="ALL">Everyone</option>
                                        <?php foreach ($apartamentos as $apartamento) : ?>

                                            <option value="<?= $apartamento->getN_Habitacion() ?>"><?= $apartamento->getN_Habitacion(); ?></option>

                                        <?php endforeach; ?>
                                    </select>
                                </div>



                                <div id="pago" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="email">Payment Status :</label>

                                    <select id="pago_" name="payment_status" id="inputState" class="form-control">
                                        <option <?php if (isset($pago) && $pago == "ALL") {
                                                    echo "selected";
                                                } ?> value="ALL">Everyone</option>
                                        <option <?php if (isset($pago) && $pago == "Pagado") {
                                                    echo "selected";
                                                } ?> value="Pagado">Paid</option>
                                        <option <?php if (isset($pago) && $pago == "NoPagado") {
                                                    echo "selected";
                                                } ?> value="NoPagado">UnPaid</option>

                                    </select>
                                </div>



                                <div id="estado" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="email">Status :</label>

                                    <select id="estado_" name="estado" id="inputState" class="form-control">
                                        <option <?php if (isset($estado) && $estado == "ALL") {
                                                    echo "selected";
                                                } ?> value="ALL">Everyone</option>
                                        <option <?php if (isset($estado) && $estado == "1") {
                                                    echo "selected";
                                                } ?> value="1">Active</option>
                                        <option <?php if (isset($estado) && $estado == "0") {
                                                    echo "selected";
                                                } ?> value="0">Cancelled</option>

                                    </select>
                                </div>

                            </div>


                            <div class="row">

                                <div class="col-xl-1 col-lg-1 col-md-3 col-sm-3 col-3 ml-3">
                                    <p><input type="submit" value="Search" class="btn btn-success"></p>
                                </div>
                                <div class="col-xl-1 col-lg-1 col-md-3 col-sm-3 col-3">
                                    <a class="btn btn-danger" href="seeker?pagina=0">Clean</a>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>

                <div class="col col-12 mt-3 table-responsive-xl">
                    <table class="table table-striped">
                        <tr class="text-white">
                            <th scope="col" class="p-2 border-right bg-RojoTable"> NºReservation</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">From</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">To</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Payment</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Name</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Surname</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Pets</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Total</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Adult</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Children</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Apartament</th>

                            <th scope="col" class="p-2 bg-RojoTable">Options</th>

                        </tr>

                        <?php


                        if (isset($reservaswithFilter)) {

                            $consulta_ssql = $_SESSION["consulta"];

                            $ssql = "$consulta_ssql " . " limit " . $inicio . "," . $TAMANO_PAGINA;
                        }


                        //construyo la sentencia SQL






                        if (empty($errores) == FALSE) {
                           
                        ?>


                                <tr class="table-active">
                                    <td colspan=12 scope="row" class="text-center">
                                       <p><?= $errores[0] ?></p> 
                                    </td>
                                </tr>

                            <?php 
                        }





                        if ($filtro_desactivado == NULL && empty($errores)) {
                            ?>
                            <tr class="table-active">
                                <td colspan=12 scope="row" class="text-center">
                                    Select the conditions to locate the reservations</p>
                            </tr><?php
                                }

                                if ($filtro_desactivado == TRUE) {


                                    $i = 1;

                                    if (isset($reservaswithFilter)) {


                                        foreach ($reservaswithFilter as $reserva) {

                                            $i++;



                                            // CAMBIAMOS DE FORMATO DE FECHA YYYY/MM/DD = DD/MM/YYYY

                                            $originalEntrada = $reserva->getFecha_Entrada();
                                            $newDateEntrada = date("d/m/Y", strtotime($originalEntrada));

                                            $originalSalida = $reserva->getFecha_Salida();
                                            $newDateSalida = date("d/m/Y", strtotime($originalSalida));



                                    ?>



                                    <tr class="table-active <?php if ($i % 2 == 0) {
                                                                echo "bg-white";
                                                            } ?> ">
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Reserva(); ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $newDateEntrada; ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $newDateSalida; ?></td>
                                        <?php

                                            if ($reserva->getEstado() == 1) {
                                                if ($reserva->getPrepago() == $reserva->getPrecio_Total()) {
                                        ?>
                                                <td scope="row" class="bg-success rounded text-white border-bottom border-dark">
                                                    Received
                                                </td>
                                            <?php
                                                } else {
                                            ?>
                                                <td scope="row" class="bg-danger rounded text-white border-bottom border-dark">
                                                    Failed
                                                </td>
                                            <?php
                                                }
                                            } else {
                                            ?>
                                            <td scope="row" class="bg-warning rounded text-white border-bottom border-dark">
                                                Cancelled
                                            </td>
                                        <?php
                                            } ?>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNombre(); ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getApellidos(); ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getMascota(); ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getPrecio_Total() . "€"; ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Adultos(); ?></td>
                                        <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Ninios(); ?></td>

                                        <?php if ($reserva->getNumero_Apartamento() == "") {
                                        ?>
                                            <td class="border-bottom border-dark" scope="row"><?= "Not Assigned"; ?></td>
                                        <?php
                                            } else { ?>
                                            <td class="border-bottom border-dark" scope="row"><?= $reserva->getNumero_Apartamento(); ?></td>
                                        <?php } 
                                        
                                        $IdReserva = $reserva->getId();
                                        ?>


                                        <td class="border-bottom border-dark" scope="row"><a href="PHP_Reservas/reservas_actualizadas?Idreserva=<?= $IdReserva ?>&&volver=seeker"><img src="images/Tables/options.png" alt=""></a></td>



                                    </tr>

                            <?php
                                        }
                                    }




                            ?>

                            <?php

                                    if (isset($reservaswithFilter)) {

                                        if (count($reservaswithFilter) >= 1) { ?>
                                    <tr>
                                        <td class="table-active pt-4" colspan=12>
                                            <strong class="">Number of Reservations Found :</strong> <?= count($reservaswithFilter) ?>
                                        </td>
                                    </tr>

                        <?php }
                                    }
                                }
                        ?>



                    </table>

                    <?php


                    //muestro los distintos índices de las páginas, si es que hay varias páginas

                    if ($filtro_desactivado == TRUE && isset($reservaswithFilter)) {
                        if ($total_paginas > 1) {
                            for ($i = 1; $i <= $total_paginas; $i++) {
                                if ($pagina == $i)
                                    //si muestro el índice de la página actual, no coloco enlace
                                    echo "<a id='a_prueba' class='btn btn-danger active' href='#'>" . $i . "</a> ";
                                else
                                    //si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página
                                    echo "<a class='btn btn-danger active' href='seeker?pagina=" . $i . "'>" . $i . "</a> ";
                            }
                        }
                    }
                    ?>

                </div>

            </div>


        </div>


    </div>


    </div>


    </div>

    <?php require __DIR__ . "/partials/fin-doc.part.php"; ?>