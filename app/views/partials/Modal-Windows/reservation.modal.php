<div class="modal" tabindex="-1" id="ventana-modal-errores">


    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Warning</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="text-dark text-left" method="POST" action="PHP_usuarios/usuarios_sql.php?opcion=borrar">


                    <?php

                    if (empty($errores) == FALSE) {
                        foreach ($errores as $error) {
                    ?>
                    <div class="form-row ">
                    <div class="form-group col-12 text-center" role="alert">
                                <?= $error ?>
                            </div>
                    </div>

                    <?php }
                    }
                    ?>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>