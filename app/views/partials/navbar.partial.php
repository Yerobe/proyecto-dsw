<?php

function indicidor_de_Pestana($pagina_actual, $pagina_mandada) // Determina la página en la que estamos para indicarlos con el indicados de color Verde a la izquierda
{

    if ($pagina_actual == $pagina_mandada) {
        echo "bg-success"; // Color Verde
    } else {
        echo "bg-navbar"; // Color Gris
    }
}






function navegacion_Lateral($pagina_actual)
{


 if(strpos($_SERVER["REQUEST_URI"],"PHP_") ){$activa = 1;}else{$activa = 0;} // Determina si se encuentra en una Subcarpeta para colocar "../" ?>



    <style>
        .dropdown-item:hover {
            background-color: #4B545E;
        }

        .dropdown-item:focus {
            background-color: #4B545E;
        }

    </style>


    <div class="col-xl-2 col-lg-12 col-md-12 col-sm-12 col-12">






        <div class="row h-100">





            <div class="col col-12 bg-navbar">

                <div class="row">
                    <div class="col col-12 bg-administracion">
                        <p class="text-center p-administracion p-3 my-auto">ADMINISTRATION</p>
                    </div>
                </div>


                <nav class="navbar navbar-light navbar-expand-sm px-0 flex-row flex-nowrap">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarWEX" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse" id="navbarWEX">

                        <div class="nav flex-xl-column flex-row ml-2">

                            <div class="row">


                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "home")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/home.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>home">Home</a>
                                    </div>
                                </div>


                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "bookings")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/calendario.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>booking">Bookings</a>
                                    </div>
                                </div>

                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "seeker")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/buscar.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>seeker?pagina=0">Seeker</a>
                                    </div>
                                </div>

                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "restaurant")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/comida.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>restaurant">Restaurant</a>
                                    </div>
                                </div>


                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "rooms")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/habitaciones.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>not-found">Rooms</a>
                                    </div>
                                </div>




                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">




                                    <div class="row">

                                        <div class="col col-12">
                                            <div id="contenido">
                                                <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "reports")  ?> float-left mr-2"></div>
                                                <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/reportes.png" alt="">
                                                <li class="active">
                                                    <a href="#reportsSubmenu" class="nav-item nav-link text-white mt-2 float-left dropdown-toggle" data-toggle="collapse" aria-expanded="false">Reports</a>

                                                    <ul class="collapse list-unstyled text-left float-left" id="reportsSubmenu">
                                                        <li><a class="dropdown-item text-white" href="<?php if($activa == 0){echo "PHP_Reportes/";} ?>reportes_ocupacion">- Occupattion</a></li>
                                                        <li><a class="dropdown-item text-white" href="<?php if($activa == 0){echo "PHP_Reportes/";} ?>reportes_checkin-checkout">- Check/IN-OUT</a></li>
                                                        <li><a class="dropdown-item text-white" href="not-found">- Regimen</a></li>
                                                        <li><a class="dropdown-item text-white" href="<?php if($activa == 1){echo "../";} ?>not-found">- Invoices</a></li>
                                                    </ul>
                                                </li>


                                            </div>
                                        </div>
                                    </div>




                                </div>

                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "today")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/hoy.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>today">Today</a>
                                    </div>
                                </div>


                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "travel")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/agent.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>not-found">Travel Agent</a>
                                    </div>
                                </div>





                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">




                                    <div class="row">

                                        <div class="col col-12">
                                            <div id="contenido">
                                                <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "facturas")  ?> float-left mr-2"></div>
                                                <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/factura.png" alt="">
                                                <li class="active">
                                                    <a href="#reportsSubmenuinvoices" class="nav-item nav-link text-white mt-2 float-left dropdown-toggle" data-toggle="collapse" aria-expanded="false">All Invoices</a>

                                                    <ul class="collapse list-unstyled text-left float-left" id="reportsSubmenuinvoices">
                                                        <li><a class="dropdown-item text-white" href="<?php if($activa == 1){echo "../";} ?>not-found">- Provider</a></li>
                                                        <li><a class="dropdown-item text-white" href="<?php if($activa == 1){echo "../";} ?>not-found">- Reservation</a></li>
                                                    </ul>
                                                </li>


                                            </div>
                                        </div>
                                    </div>




                                </div>




                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <div>
                                        <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "help")  ?> float-left mr-2"></div>
                                        <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/ayuda.png" alt="">
                                        <a class="nav-item nav-link text-white mt-2 float-left" href="<?php if($activa == 1){echo "../";} ?>not-found">Help</a>
                                    </div>
                                </div>




                                <div class="col-xl-12 col-lg-3 col-md-3 col-sm-6 col-12">
                                    
                                   

                                        <div class="row">

                                            <div class="col col-12">
                                                <div id="contenido">
                                                    <div style="width: 5px; height:48px;" class="<?php indicidor_de_Pestana($pagina_actual, "administration")  ?> float-left mr-2"></div>
                                                    <img class="float-left mt-3" src="<?php if($activa == 1){echo "../";} ?>images/NavBar/administracion.png" alt="">
                                                    <li class="active">
                                                        <a href="#homeSubmenu" class="nav-item nav-link text-white mt-2 float-left nav-link dropdown-toggle" data-toggle="collapse" aria-expanded="false">Configure</a>

                                                        <ul class="collapse list-unstyled text-left  float-left" id="homeSubmenu">
                                                            <li><a class="dropdown-item text-white" href="<?php if($activa == 1){echo "../";} ?>not-found">- Users</a></li>
                                                            <li><a class="dropdown-item text-white" href="<?php if($activa == 1){echo "../";} ?>not-found">- Companies</a></li>
                                                            <li><a class="dropdown-item text-white" href="<?php if($activa == 1){echo "../";} ?>not-found">- Provider</a></li>
                                                        </ul>
                                                    </li>


                                                </div>
                                            </div>
                                        </div>




                                 
                                </div>

















                            </div>




                        </div>
                    </div>
                </nav>
            </div>
        </div>





    </div>

<?php
} ?>