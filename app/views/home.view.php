<?php require __DIR__ . "/../views/partials/inicio-doc.part.php"; ?>



</head>

<body id="page-top" class="fondo">

<?php require __DIR__ . "/../views/partials/topbar.partial.php"; ?>


<div class="row alto">

    <?php navegacion_Lateral($pagina_actual = "home"); ?>


    <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12">

        <div class="col col-12 bg-white mt-5 p-3" id="Rooms_Border">

            <div class="row">
                <div class="col col-12 text-center">
                    <a href="home?dia=yesterday" class="text-dark display-4 blockquote text-decoration-none">
                        <</a> <a href="home" class="text-dark display-4 blockquote text-decoration-none">Today
                    </a>
                    <a href="home?dia=tomorrow" class="text-dark display-4 blockquote text-decoration-none">></a>
                </div>
            </div>


            <div class="col col-12 mt-3 table-responsive-xl">
                <div class="row border-bottom border-dark mb-3">
                    <div class="col col-12">
                        <h3 class="display-4 blockquote"><?php if ($_GET["dia"] == "yesterday") {
                                                                echo "Yesterday Check-In";
                                                            } elseif ($_GET["dia"] == "tomorrow") {
                                                                echo "Tomorrow Check-In";
                                                            } else {
                                                                echo "Today Check-In";
                                                            } ?></h3>
                    </div>
                </div>

                <table id="table" class="table">
                    <tr class="text-white">
                        <th scope="col" class="p-2 border-right bg-RojoTable"> NºReservation</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">From</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">To</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Payment</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Name</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Surname</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Pets</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Total</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Adult</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Children</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Apartament</th>

                        <th scope="col" class="p-2 bg-RojoTable">Options</th>

                    </tr>

                 <?php if(empty($reservas_checkin) && empty($errores)){ ?>
                           
                        <tr class="table-active">
                            <td colspan=12 scope="row" class="text-center">
                                Today there are no entries</p>
                        </tr>

                 <?php } ?>

                           
                        <?php 
                        if(empty($errores)){

                        foreach($reservas_checkin as $reserva){ 
                            
                            $originalEntrada = $reserva->getFecha_Entrada();
                            $newDateEntrada = date("d/m/Y", strtotime($originalEntrada)); // Cambiamos el formato de fecha YYYY/MM/DD = DD/MM/YYYY

                            $originalSalida = $reserva->getFecha_Salida();
                            $newDateSalida = date("d/m/Y", strtotime($originalSalida)); // Cambiamos el formato de fecha YYYY/MM/DD = DD/MM/YYYY



                            ?>

                        <tr class="table-active ">
                            <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Reserva(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo $newDateEntrada; ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo $newDateSalida; ?></td>
                            
                            <?php if($reserva->getPrepago() == $reserva->getPrecio_Total()){ ?>
                                <td scope="row" class="bg-success rounded text-white border-bottom border-dark">
                                    Received
                            </td> <?php }else{ ?>
                          
                                <td scope="row" class="bg-danger rounded text-white border-bottom border-dark">
                                    Failed
                            </td> <?php } ?>
                           
                            <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNombre(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getApellidos(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getMascota(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getPrecio_Total() . "€"; ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getNumero_Adultos(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getNumero_Ninios(); ?></td>

                            <?php if($reserva->getNumero_Apartamento() == NULL){ ?>
                           
                            <td class="border-bottom border-dark" scope="row"><?php echo "Arrival"; ?></td> <?php }else{ ?>
                           
                            <td class="border-bottom border-dark" scope="row"><?php  $reserva->getNumero_Apartamento(); ?></td> <?php } ?>

                           

                            <td class="border-bottom border-dark" scope="row"><a href="PHP_Reservas/reservas_actualizadas?Idreserva=<?= $reserva->getId() ?>&&volver=home"><img src="images/Tables/options.png" alt=""></a></td>


                        </tr>

                        <?php }}else{?>
                            <tr class="table-active text-center">
                            <td colspan="12" class="border-bottom border-dark" scope="row"><?= $errores[0] ?></td>
                            </tr>
                        <?php
                        } ?>

              

                </table>

            </div>


            <div class="col col-12 mt-3 table-responsive-xl mt-5">
                <div class="row border-bottom border-dark mb-3">
                    <div class="col col-12">
                        <h3 class="display-4 blockquote"><?php if ($_GET["dia"] == "yesterday") {
                                                                echo "Yesterday Check-OUT";
                                                            } elseif ($_GET["dia"] == "tomorrow") {
                                                                echo "Tomorrow Check-OUT";
                                                            } else {
                                                                echo "Today Check-OUT";
                                                            } ?></h3>
                    </div>
                  
                </div>
                <table id="table" class="table">
                    <tr class="text-white">
                        <th scope="col" class="p-2 border-right bg-RojoTable"> NºReservation</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">From</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">To</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Payment</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Name</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Surname</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Pets</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Total</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Adult</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Children</th>
                        <th scope="col" class="p-2 border-right bg-RojoTable">Apartament</th>

                        <th scope="col" class="p-2 bg-RojoTable">Options</th>

                    </tr>

                    <?php if(empty($reservas_checkout) && empty($errores)){ ?>
                           
                           <tr class="table-active">
                               <td colspan=12 scope="row" class="text-center">
                                   Today there are no check out</p>
                           </tr>
   
                    <?php } 

                           
                        
                   
                        if(empty($errores)){

                        foreach($reservas_checkout as $reserva){ 
                            
                            $originalEntrada = $reserva->getFecha_Entrada();
                            $newDateEntrada = date("d/m/Y", strtotime($originalEntrada)); // Cambiamos el formato de fecha YYYY/MM/DD = DD/MM/YYYY

                            $originalSalida = $reserva->getFecha_Salida();
                            $newDateSalida = date("d/m/Y", strtotime($originalSalida)); // Cambiamos el formato de fecha YYYY/MM/DD = DD/MM/YYYY



                            ?>

                        <tr class="table-active ">
                            <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Reserva(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo $newDateEntrada; ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo $newDateSalida; ?></td>
                            
                            <?php if($reserva->getPrepago() == $reserva->getPrecio_Total()){ ?>
                                <td scope="row" class="bg-success rounded text-white border-bottom border-dark">
                                    Received
                            </td> <?php }else{ ?>
                          
                                <td scope="row" class="bg-danger rounded text-white border-bottom border-dark">
                                    Failed
                            </td> <?php } ?>
                           
                            <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNombre(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getApellidos(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getMascota(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getPrecio_Total() . "€"; ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getNumero_Adultos(); ?></td>
                            <td class="border-bottom border-dark" scope="row"><?php echo  $reserva->getNumero_Ninios(); ?></td>

                            <?php if($reserva->getNumero_Apartamento() == NULL){ ?>
                           
                            <td class="border-bottom border-dark" scope="row"><?php echo "Arrival"; ?></td> <?php }else{ ?>
                           
                            <td class="border-bottom border-dark" scope="row"><?php  $reserva->getNumero_Apartamento(); ?></td> <?php } ?>

                           
                         
                            <td class="border-bottom border-dark" scope="row"><a href="PHP_Reservas/reservas_actualizadas?Idreserva=<?= $reserva->getId() ?>&&volver=home"><img src="images/Tables/options.png" alt=""></a></td>


                        </tr>

                        <?php } }else{?>
                            <tr class="table-active text-center">
                            <td colspan="12" class="border-bottom border-dark" scope="row"><?= $errores[0] ?></td>
                            </tr>
                        <?php
                        } ?>

              

                </table>



            </div>


            <div class="row p-3">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
                    <a class="btn btn-success btn-block mt-2" href="PHP_Reservas/reservas_nuevas?volver=home">New Reservation</a>
                </div>
            </div>


        </div>


    </div>


</div>

<?php require __DIR__ . "/partials/fin-doc.part.php"; ?>