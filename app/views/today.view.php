<?php 
include __DIR__ . "../../views/partials/navbar.partial.php"; // Importación del Nav Lateral - OJO - va por funciones
require __DIR__ . "/../views/partials/inicio-doc.part.php"; 
?>

</head>


<body id="page-top" class="fondo">




    <?php require __DIR__ . "/../views/partials/topbar.partial.php"; ?> 


    <div class="row alto">

        <?php navegacion_Lateral($pagina_actual = "today"); ?>


        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mt-5">


            <div class="row mt-5">
                <div class="col col-12">
                    <h3 style="font-size: 40px;" class="display-3 blockquote">
                        Report of the Day</h3>
                    </h3>
                    <hr class="border-top border-dark">
                </div>
            </div>



            <div class="col col-12  mt-3 p-3">


                <div class="row bg-white p-3 align-content-around justify-content-around">


                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12 mb-3">
                        <div class="row">
                            <div class="col col-12  bg-secondary">

                                <?php

                                try{

                                $sql = "WHERE Estado = 1 AND Fecha_Entrada <= '$fecha' AND Fecha_Salida >= '$fecha' AND Numero_Apartamento IS NOT NULL";
                                $registro_ocupacion = $reservasRepository->findallWithWhere($sql); // llamada a función del QueryBuilder

                                $cantidad = ceil((count($registro_ocupacion) * 100) / 81); // Calculamos el Porcetaje de Ocupación, Cogiendo 81 Apartamentos como el Máximo

                                ?>
                                <p class="text-center font-weight-bold my-auto">Actual Occupation</p>
                            </div>
                            <div style="background-color: #E72588;" class="col col-12 rounded-bottom">
                                <h1 class="text-center p-5"><?php echo $cantidad . "%"; ?></h1>
                            </div>
                        </div>
                    </div>



                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12 mb-3">
                        <div class="row">
                            <div class="col col-12  bg-secondary">
                                <?php

                                $sql = "WHERE Estado = 1 AND Fecha_Entrada <= '$fecha' AND Fecha_Salida >= '$fecha'";
                                $registro_ocupacion = $reservasRepository->findallWithWhere($sql);  // llamada a función del QueryBuilder

                                $cantidad = ceil((count($registro_ocupacion) * 100) / 81); // Porcentaje de Ocupación esperada el día de hoy


                                ?>
                                <p class="text-center font-weight-bold my-auto">Expected Occupation</p>
                            </div>
                            <div style="background-color: #EC4058;" class="col col-12 rounded-bottom">
                                <h1 class="text-center p-5"><?php echo $cantidad . "%"; ?></h1>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12 mb-3">
                        <div class="row">
                            <div class="col col-12  bg-secondary">

                                <?php


                                $registro_entrada = $reservasRepository->finReservationCheckInforDay($fecha); // Entradas con día específicado
                                $numero_entradas = count($registro_entrada);



                                $registro_salida = $reservasRepository->finReservationCheckOutforDay($fecha); // Salida con día especificado
                                $numero_salidas = count($registro_salida);

                                ?>


                                <p class="text-center font-weight-bold my-auto">CheckIN / CheckOUT</p>
                            </div>
                            <div style="background-color: #409AEC;" class="col col-12 rounded-bottom">
                                <h1 class="text-center pb-5 pt-5"><?php echo $numero_entradas; ?> / <?php echo $numero_salidas; ?></h1>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12 mb-3">
                        <div class="row">
                            <div class="col col-12  bg-secondary">

                                <?php

                                $contador_estandar = $reservasRepository->findRoomsbyCategory($fecha, "Normal"); // Apartamentos normales ocupados
                                $row_estandar = count($contador_estandar);


                                $contador_superiores = $reservasRepository->findRoomsbyCategory($fecha, "Superior"); // Apartamentos superiores ocupados
                                $row_superiores = count($contador_superiores);



                                ?>
                                <p class="text-center font-weight-bold my-auto">Standar / Suites </p>
                            </div>
                            <div style="background-color: #DA801A;" class="col col-12 rounded-bottom">
                                <h1 class="text-center p-5"><?php echo $row_estandar; ?> / <?php echo $row_superiores ?></h1>
                            </div>
                        </div>
                    </div>


                    <div class="col-xl-2 col-lg-4 col-md-4 col-sm-4 col-12 mb-3">
                        <div class="row">
                            <div class="col col-12 bg-secondary">

                                <?php

                                $sql = "WHERE Estado = 1 AND Fecha_Entrada = '$fecha' AND Numero_Apartamento IS NULL";
                                $contador_llegadas_pendientes = $reservasRepository->findallWithWhere($sql); // Apartamentos pendientes de llegar
                                $row_llegadas_pendientes = count($contador_llegadas_pendientes);



                                ?>



                                <p class="text-center font-weight-bold my-auto">Waiting Check-In</p>
                            </div>
                            <div style="background-color: #6ED746;" class="col col-12 rounded-bottom">
                                <h1 class="text-center p-5"><?php echo $row_llegadas_pendientes; ?></h1>
                            </div>
                        </div>
                    </div>




                </div>

            </div>




            <div class="col col-12 bg-white mt-5 p-3" id="Rooms_Border">
                <div class="row p-3">





                    <div class="col col-12 mt-3">




                        <form style="background-color: #F8FBF8;" id="miForm" action="today" method="POST" class="border rounded border-dark">
                            <div class="form-group row p-3 ">



                                <div id="fecha" class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="fecha">Date</label>
                                    <input value="<?php echo $fecha ?>" id="fecha" name="fecha" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-3 col-lg-2 col-md-3 col-sm-3 col-3 ml-3">
                                    <p><input style="margin-top: 35px;" type="submit" value="Search" class="btn btn-success btn-block"></p>
                                </div>
                                <div class="col-xl-3 col-lg-2 col-md-3 col-sm-3 col-3">
                                    <a style="margin-top: 35px;" class="btn btn-danger btn-block " href="today">Clean</a>
                                </div>


                            </div>


                        </form>
                    </div>
                </div>

                <div class="col col-12 mt-3 table-responsive-xl">
                    <table class="table table-striped mt-3">
                        <tr class="text-white">
                            <th scope="col" class="p-2 border-right bg-RojoTable"> NºReservation</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">From</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">To</th>

                            <th scope="col" class="p-2 border-right bg-RojoTable">Name</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Surname</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Pets</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Total</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Payment</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Adult</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Children</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Apartament</th>

                            <th scope="col" class="p-2 bg-RojoTable">Options</th>

                        </tr>

                        <?php


                        //construyo la sentencia SQL
                        $sql = " WHERE Estado = 1 AND Fecha_Entrada <= '$fecha' AND Fecha_Salida >= '$fecha' ORDER BY Fecha_Entrada ASC";
                        $reservas_totales = $reservasRepository->findallWithWhere($sql); // Todas las reservas que se encuentren dentro del hotel en la fecha indicada

                    }catch (PDOException $PDOException) {

                        $errores[] = "Error en el SCRIPT de la BBDD";
                    }
                        

                        if (isset($reservas_totales) && count($reservas_totales) == 0) { // En el caso de que no hayan apartamentos

                        ?>
                            <tr class="table-active">
                                <td colspan=12 scope="row" class="text-center">

                                    Today there is no occupied apartment</p>
                            </tr><?php
                                }
                                $i = 0;



                                if(isset($reservas_totales)){
                                foreach ($reservas_totales as $reserva) { // Mostramos todas las reservas obtenidas


                                    $i++;

                                    // CAMBIAMOS DE FORMATO DE FECHA YYYY/MM/DD = DD/MM/YYYY

                                    $originalEntrada = $reserva->getFecha_Entrada();
                                    $newDateEntrada = date("d/m/Y", strtotime($originalEntrada));

                                    $originalSalida = $reserva->getFecha_Salida();
                                    $newDateSalida = date("d/m/Y", strtotime($originalSalida));


                                    $dia_despues =  strtotime($fecha . "+ 1 days");



                                    ?>



                            <tr class="table-active <?php if ($i % 2 == 0) {
                                                        echo "bg-white";
                                                    } ?>">
                                <td class="border border-dark <?php  ?>" scope="row"><?php echo $reserva->getNumero_Reserva(); ?></td>
                                <td class="border border-dark <?php if ($fecha == $reserva->getFecha_Entrada()) {
                                                                    echo "bg-success text-white";
                                                                } ?>" scope="row"><?php echo $newDateEntrada; ?></td>
                                <td class="border border-dark <?php if (date("Y-m-d", $dia_despues) == $reserva->getFecha_Salida()) {
                                                                    echo "bg-primary text-white";
                                                                } elseif ($fecha == $reserva->getFecha_Salida()) {
                                                                    echo "bg-danger text-white";
                                                                } ?>" scope="row"><?php echo $newDateSalida; ?></td>

                                <td class="border border-dark" scope="row"><?php echo $reserva->getNombre(); ?></td>
                                <td class="border border-dark" scope="row"><?php echo $reserva->getApellidos(); ?></td>
                                <td class="border border-dark" scope="row"><?php echo $reserva->getMascota(); ?></td>
                                <td class="border border-dark" scope="row"><?php echo $reserva->getPrecio_Total() . "€"; ?></td>
                                <?php if ($reserva->getPrepago() == $reserva->getPrecio_Total()) {
                                ?>
                                    <td scope="row" class="bg-success rounded text-white border border-dark">
                                        Received
                                    </td>
                                <?php
                                    } else {
                                ?>
                                    <td scope="row" class="bg-danger rounded text-white border border-dark">
                                        Failed
                                    </td>
                                <?php
                                    } ?>
                                <td class="border border-dark" scope="row"><?php echo $reserva->getNumero_Adultos(); ?></td>
                                <td class="border border-dark" scope="row"><?php echo $reserva->getNumero_Ninios(); ?></td>

                                <td class="border border-dark <?php if ($reserva->getNumero_Apartamento() == 0) {
                                                                    echo "bg-danger";
                                                                } ?>" scope="row"><?php echo $reserva->getNumero_Apartamento(); ?></td>


                                <td class="border border-dark" scope="row"><a href="#"><img src="images/Tables/options.png" alt=""></a></td>



                            </tr>

                        <?php
                                 }}
                        ?>



                    </table>

                   <?php if(isset($reservas_totales)){ ?> 

                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                            <p class="bg-danger text-center font-weight-bold border border-dark">Today Check-OUT</p>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                            <p class="bg-primary text-center font-weight-bold border border-dark">Tomorrow Check-OUT</p>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6">
                            <p class="bg-success text-center font-weight-bold border border-dark">Today Check-IN</p>
                        </div>
                    </div>

                   <?php }else{
                       ?>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <p class="bg-danger text-center font-weight-bold border border-dark p-3 m-3"><?= $errores[0] ?></p>
                        </div>

                    </div>
                       <?php
                   } ?>



                </div>

            </div>


        </div>


    </div>


    </div>


    </div>

    <?php require __DIR__ . "/partials/fin-doc.part.php"; ?>