<!DOCTYPE html>
<html lang="en">

<head>
    <title>La Piramide</title>

    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="Index-Content/images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="Index-Content/css/util.css">
    <link rel="stylesheet" type="text/css" href="Index-Content/css/main.css">
    <!--===============================================================================================-->



</head>

<body>



    <div class="limiter">
        <div class="container-login100" style="background-image: url('Index-Content/images/bg-01.jpg');">
            <div class="wrap-login100 p-t-30 p-b-50">
                <span class="login100-form-title p-b-41">
                    Account Login
                </span>
                <form class="login100-form validate-form p-b-33 p-t-5" action="" method="POST">

                    <div class="wrap-input100 validate-input" data-validate="Enter username">
                        <input value="" class="input100" type="text" name="usuario" placeholder="User name" pattern="[A-Za-z0-9_-]{1,25}">
                        <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input id="password-field" class="input100" type="password" name="password" placeholder="Password" onkeypress="capLock(event)" pattern="[A-Za-z0-9_-]{1,25}">
                        <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                    </div>

                    <div id="divMayus" style="visibility:hidden">
                        <p class="text-center mt-2">Caps Lock is on.</p>
                    </div>





            

                    <?php

                    if (empty($errores) == FALSE) {
                        foreach ($errores as $error) {
                    ?>
                            <div class="alert alert-danger text-center" role="alert">
                                <?= $error ?>
                            </div>
                    <?php }
                    }
                    ?>



                    <div class="container-login100-form-btn m-t-32">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <div id="dropDownSelect1"></div>






    <!--===============================================================================================-->
    <script src="Index-Content/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/vendor/bootstrap/js/popper.js"></script>
    <script src="Index-Content/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/vendor/daterangepicker/moment.min.js"></script>
    <script src="Index-Content/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/js/main.js"></script>
    <!--===============================================================================================-->
    <script src="Index-Content/js/capLock.js"></script>






</body>

</html>