<?php require __DIR__ . "/../views/partials/inicio-doc.part.php"; ?>



</head>

<body id="page-top" class="fondo">

    <?php require __DIR__ . "/../views/partials/topbar.partial.php"; ?>


    <div class="row alto">

        <?php navegacion_Lateral($pagina_actual = "bookings"); ?>


        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mt-5">

        <div class="row ">
                <div class="col col-12">
                    <h3 style="font-size: 40px;" class="display-3 blockquote">Booking</h3>
                    <hr class="border-top border-dark">
                </div>
            </div>

            <div class="col col-12 text-white">

                <div class="row">

                    <div class="col col-12 mt-5 bg-negro h-100 rounded">

                        <div class="row">
                            <div class="col col-12 mt-3 mb-3">
                                <a href="PHP_Reservas/reservas_nuevas?volver=booking" class="text-decoration-none text-white">Reservation List</a>

                            </div>
                        </div>

                    </div>

                    <div class="col col-12  table-responsive-xl bg-white">
                        <table class="table table-striped mt-5">
                            <tr class="text-white">
                                <th scope="col" class="p-2 border-right bg-RojoTable"> NºReservation</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">From</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">To</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Payment</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Name</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Surname</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Pets</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Total</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Adult</th>
                                <th scope="col" class="p-2 border-right bg-RojoTable">Children</th>
                                <th scope="col" class="p-2 bg-RojoTable">Options</th>
                            </tr>

                            <?php

                          

                             if(empty($reservas_bookday) && empty($errores)) {

                                        ?>
                                <tr class="table-active">
                                    <td colspan=11 scope="row" class="text-center">
                                        You have not entered reservations today</p>
                                </tr><?php
                                    }elseif(!empty($errores)){
                                        ?>
                                <tr class="table-active">
                                    <td colspan=11 scope="row" class="text-center">
                                        <?= $errores[0] ?></p>
                                </tr><?php
                                    }
                                    $i = 1;

                                    if(empty($errores)){

                                    foreach($reservas_bookday as $reserva){ 

                                        $i++;

                                        // CAMBIAMOS DE FORMATO DE FECHA YYYY/MM/DD = DD/MM/YYYY

                                        $originalEntrada = $reserva->getFecha_Entrada();
                                        $newDateEntrada = date("d/m/Y", strtotime($originalEntrada));

                                        $originalSalida = $reserva->getFecha_Salida();
                                        $newDateSalida = date("d/m/Y", strtotime($originalSalida));

                                        ?>

                                <tr class="table-active <?php if ($i % 2 == 0) {
                                                            echo "bg-white";
                                                        } ?> ">
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Reserva(); ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $newDateEntrada; ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $newDateSalida; ?></td>

                                    <?php if ($reserva->getPrepago() == $reserva->getPrecio_Total()) {
                                    ?>
                                        <td scope="row" class="bg-success rounded text-white border-bottom border-dark">
                                            Received
                                        </td>
                                    <?php
                                        } else {
                                    ?>
                                        <td scope="row" class="bg-danger rounded text-white border-bottom border-dark">
                                            Failed
                                        </td>
                                    <?php
                                        } ?>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNombre(); ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getApellidos(); ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getMascota(); ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getPrecio_Total() . "€"; ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Adultos(); ?></td>
                                    <td class="border-bottom border-dark" scope="row"><?php echo $reserva->getNumero_Ninios(); ?></td>
                                    <td class="border-bottom border-dark" scope="row"><a href="PHP_Reservas/reservas_actualizadas?Idreserva=<?= $reserva->getId() ?>&&volver=booking"><img src="images/Tables/options.png" alt=""></a></td>
                                </tr>

                            <?php
                                    }}

                                    

                            ?>

                            <?php




                            
                            if (isset($reservas_bookday) && count($reservas_bookday) >= 1) { ?>
                                <tr>
                                    <td class="table-active pt-4" colspan=11>
                                        <strong class="">Number of Reservations Found :</strong> <?php echo count($reservas_bookday) ?>

                                    </td>
                                </tr>

                            <?php } ?>

                        </table>

                    </div>

                </div>

            </div>


        </div>


    </div>


    </div>

    <?php require __DIR__ . "/partials/fin-doc.part.php"; ?>