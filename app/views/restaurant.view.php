<?php include __DIR__ . "../../views/partials/navbar.partial.php"; // Importación del Nav Lateral 
?>

<?php require __DIR__ . "/../views/partials/inicio-doc.part.php"; ?>


<script src="js/date-min.js"></script>


</head>


<body id="page-top" class="fondo">




    <?php require __DIR__ . "/../views/partials/topbar.partial.php"; ?>


    <div class="row alto">

        <?php navegacion_Lateral($pagina_actual = "restaurant"); ?>


        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mt-5">


            <div class="row mt-5">
                <div class="col col-12">
                    <h3 style="font-size: 40px;" class="display-3 blockquote">
                        Restaurant Information Query</h3>
                    </h3>
                    <hr class="border-top border-dark">
                </div>
            </div>


            <div class="col col-12 bg-white mt-5 p-3" id="Rooms_Border">
                <div class="row p-3">





                    <div class="col col-12 mt-3">




                        <form style="background-color: #F8FBF8;" id="miForm" action="restaurant" method="POST" class="border rounded border-dark">
                            <div class="form-group row p-3 ">



                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="desde">From</label>
                                    <input onchange="myFunction()" id="desde" value="<?php echo @$desde ?>" name="desde" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="hasta">To</label>
                                    <input id="hasta" value="<?php echo @$hasta ?>" name="hasta" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-3 ml-3">
                                    <p><input style="margin-top: 35px;" type="submit" value="Search" class="btn btn-success btn-block"></p>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-3">
                                    <a style="margin-top: 35px;" class="btn btn-danger btn-block " href="restaurant">Clean</a>
                                </div>


                            </div>


                        </form>
                    </div>
                </div>

                <div class="col col-12 mt-3 table-responsive-xl">
                    <table class="table table-striped">
                        <tr class="text-white">
                            <th scope="col" class="p-2 border-right bg-RojoTable"> Date</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Rooms</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Adults</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Children</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Breakfast</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Lunch</th>
                            <th scope="col" class="p-2 border-right bg-RojoTable">Dinner</th>

                        </tr>

                        <?php



                        //construyo la sentencia SQL





                        if ($filtro_desactivado == NULL) {
                        ?>
                            <tr class="table-active">
                                <td colspan=7 scope="row" class="text-center">
                                    Select the conditions to locate the reservations</p>
                            </tr><?php
                                }



                                if ($filtro_desactivado == TRUE) {





                                    for ($i = 0; date($mod_date) <= date($hasta_mod); $mod_date = strtotime($date . "+ $i days")) { // Se repetira el bucle hasta que desde alcance hasta sumandole un día a desde



                                        $i++; // Lo utilizamos para el Bg de los tr


                                        $date_2 = date("Y-m-d", $mod_date); // Registro la Variable para la consulta


                                        // Consulta de Reservas por Dia

                                        try {
                                            $reservas_totales = $reservasRepository->findallWithDay($date_2);
                                        } catch (PDOException $PDOException) {
                                            $errores[] = "Error en el SCRIPT";
                                        }

                                        if (isset($reservas_totales)) {


                                            // Inicializamos los contadores a 0 para que no se acumulen
                                            
                                            $adultos_totales = 0;
                                            $ninios_totales = 0;
                                            $desayunos_totales = 0;
                                            $almuerzos_totales = 0;
                                            $cenas_totales = 0;
                                            $rooms_totales = count($reservas_totales);

                                            for ($j = 0; $j < count($reservas_totales); $j++) {

                                                $sumatorio_adultos = $reservas_totales[$j]->getNumero_Adultos();
                                                $adultos_totales = $adultos_totales + $sumatorio_adultos;

                                                $sumatorio_ninios = $reservas_totales[$j]->getNumero_Ninios();
                                                $ninios_totales = $ninios_totales + $sumatorio_ninios;


                                                /* Calculamos los Breakfast */

                                                if ($reservas_totales[$j]->getPension() != "Only Bed" && $reservas_totales[$j]->getFecha_Entrada() != $date_2) { // Cualquier regimen exepto Only Bed son Desayunos, deberemos de tener en cuenta que si la reserva entra hoy, hoy no tiene desayuno ya que este es de 08:00 - 10:00
                                                    $desayunos_totales = $desayunos_totales + $reservas_totales[$j]->getNumero_Adultos() + $reservas_totales[$j]->getNumero_Ninios();
                                                }

                                                /* Calculamos los Almuerzos */

                                                if ($reservas_totales[$j]->getPension() == "Full Board") {
                                                    $almuerzos_totales = $almuerzos_totales + $reservas_totales[$j]->getNumero_Adultos() + $reservas_totales[$j]->getNumero_Ninios();
                                                }

                                                /* Calculamos las Cenas */

                                                if (($reservas_totales[$j]->getPension() == "Full Board" || $reservas_totales[$j]->getPension() == "Half Board") && $reservas_totales[$j]->getFecha_Salida() != $date_2) {
                                                    $cenas_totales = $cenas_totales + $reservas_totales[$j]->getNumero_Adultos() + $reservas_totales[$j]->getNumero_Ninios();
                                                }
                                            }




                                    ?>


                                    <tr class="table-active <?php if ($i % 2 == 0) {
                                                                echo "bg-white";
                                                            } ?>">
                                        <td class="border border-dark" scope="row"><?php echo date("d-m-Y", $mod_date); ?></td>
                                        <td class="border border-dark" scope="row"><?= $rooms_totales; ?></td>
                                        <td class="border border-dark" scope="row"><?= $adultos_totales; ?></td>
                                        <td class="border border-dark" scope="row"><?= $ninios_totales; ?></td>
                                        <td class="border border-dark" scope="row"><?= $desayunos_totales; ?></td>
                                        <td class="border border-dark" scope="row"><?= $almuerzos_totales; ?></td>
                                        <td class="border border-dark" scope="row"><?= $cenas_totales; ?></td>

                                    </tr>



                                <?php


                                        } else {
                                ?>
                                    <tr class="table-active">



                                        <?php

                                            if (!isset($mensaje_enviado)) {
                                                $mensaje_enviado = TRUE;

                                                foreach ($errores as $error) {
                                        ?>
                                                <td colspan="7" class="border border-dark text-center bg-gray"> <?= $error ?> </td>
                                        <?php }
                                            }



                                        ?>
                                    </tr>
                        <?php
                                        }
                                    }
                                }


                        ?>



                    </table>



                </div>

            </div>

            <div class="col col-12 mt-5">



            </div>



        </div>


    </div>


    </div>


    </div>

    <?php require __DIR__ . "/partials/fin-doc.part.php"; ?>