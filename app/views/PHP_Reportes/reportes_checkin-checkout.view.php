<?php include __DIR__ . "/../../views/partials/navbar.partial.php"; // Importación del Nav Lateral 
require __DIR__ . "/../../views/partials/inicio-doc.part.php"; ?>



<script src="../js/date-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>



</head>

<body id="page-top" class="fondo">




    <?php require __DIR__ . "/../../views/partials/topbar.partial.php"; // Barra Superior 
    ?>


    <div class="row alto">

        <?php navegacion_Lateral($pagina_actual = "reports"); ?>


        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mt-5">


            <div class="row mt-5">
                <div class="col col-12">
                    <h3 style="font-size: 40px;" class="display-3 blockquote">Reports of Inputs and Outputs</h3>
                    <hr class="border-top border-dark">
                </div>
            </div>



            <div class="col col-12 bg-white mt-5 p-3" id="Rooms_Border">



                <div class="row p-3">





                    <div class="col col-12 mt-3">




                        <form style="background-color: #F8FBF8;" id="miForm" action="reportes_checkin-checkout" method="POST" class="border rounded border-dark">
                            <div class="form-group row p-3 ">



                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="desde">From</label>
                                    <input onchange="myFunction()" value="<?php echo $desde ?>" id="desde" name="desde" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="hasta">To</label>
                                    <input value="<?php echo $hasta ?>" id="hasta" name="hasta" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-3 ml-3">
                                    <p><input style="margin-top: 35px;" type="submit" value="Search" class="btn btn-success btn-block"></p>
                                </div>

                            </div>


                        </form>
                    </div>
                </div>



                <div class="col col-12 ">
                    <?php if ($filtro_desactivado == TRUE) { ?>
                        <canvas id="myChart"> </canvas>
                    <?php } else {
                    ?>
                        <p class="font-weight-bold text-center p-3">Please select estimated dates</p>
                    <?php
                    } ?>

                </div>




            </div>







        </div>


    </div>



    <?php if($_SERVER["REQUEST_METHOD"] == "POST"){ ?>

    <script>
        var canvas = document.getElementById("myChart");
        var ctx = canvas.getContext('2d');

        // Global Options:
        Chart.defaults.global.defaultFontColor = 'black';
        Chart.defaults.global.defaultFontSize = 16;

        <?php
        $date = date($desde); // Introducimos el valor del primer Input
        $hasta_mod = date($hasta); // Introducimos el valor del segundo Input


        $mod_date = strtotime($date . "+ 0 days"); // Aplicamos el formato e inicializamos la variable, ya que si no la iniciamos el for fallará
        $hasta_mod = strtotime($hasta . "+ 0 days");
        ?>


        var data = {
            labels: [<?php for ($i = 0; date($mod_date) <= date($hasta_mod); $mod_date = strtotime($date . "+ $i days")) {
                            $i++; ?> '<?php $fecha = date("d/m/Y", $mod_date);
                                            echo $fecha; ?> '
                <?php if (date($mod_date) < date($hasta_mod)) {
                                echo ",";
                            }
                        } ?>,
            ],
            datasets: [{
                    label: "Outputs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(225,0,0,0.4)",
                    borderColor: "red", // The main line color
                    borderCapStyle: 'square',
                    borderDash: [], // try [5, 15] for instance
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "black",
                    pointBackgroundColor: "white",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    pointHoverBackgroundColor: "yellow",
                    pointHoverBorderColor: "brown",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: true
                    data: [<?php


                            $date = date($desde); // Introducimos el valor del primer Input
                            $hasta_mod = date($hasta); // Introducimos el valor del segundo Input


                            $mod_date = strtotime($date . "+ 0 days"); // Aplicamos el formato e inicializamos la variable, ya que si no la iniciamos el for fallará
                            $hasta_mod = strtotime($hasta . "+ 0 days");

                            for ($i = 0; date($mod_date) <= date($hasta_mod); $mod_date = strtotime($date . "+ $i days")) {

                                $i++;

                                $date_2 = date("Y-m-d", $mod_date); 

                                $sql = "WHERE Estado = 1 AND Fecha_Salida = '$date_2'";
                                $salidas = $reservasRepository->findallWithWhere($sql);
                                $ocupacion = count($salidas);
                            ?>

                            '<?php if ($ocupacion == 0) {
                                    echo "0";
                                } else {
                                    echo $ocupacion;
                                } ?>' // Imprimimos el valor en JS

                        <?php
                                if (date($mod_date) < date($hasta_mod)) {
                                    echo ",";
                                } // Nos permite imprimir la coma que separa los valores hasta el último dato
                            }
                        ?>
                    ],
                    spanGaps: true,
                }, {
                    label: "Inputs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(225,0,0,0.4)",
                    borderColor: "rgb(0, 0, 255)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "white",
                    pointBackgroundColor: "black",
                    pointBorderWidth: 1,
                    pointHoverRadius: 8,
                    //pointHoverBackgroundColor: "brown",
                    pointHoverBorderColor: "yellow",
                    pointHoverBorderWidth: 2,
                    pointRadius: 4,
                    pointHitRadius: 10,
                    // notice the gap in the data and the spanGaps: false
                    data: [<?php


                            $date = date($desde); // Introducimos el valor del primer Input
                            $hasta_mod = date($hasta); // Introducimos el valor del segundo Input


                            $mod_date = strtotime($date . "+ 0 days"); // Aplicamos el formato e inicializamos la variable, ya que si no la iniciamos el for fallará
                            $hasta_mod = strtotime($hasta . "+ 0 days");

                            for ($i = 0; date($mod_date) <= date($hasta_mod); $mod_date = strtotime($date . "+ $i days")) {

                                $i++;

                                $date_2 = date("Y-m-d", $mod_date); // Registro la Variable para la consulta


                                $sql = "WHERE Estado = 1 AND Fecha_Entrada = '$date_2'";
                                $entradas = $reservasRepository->findallWithWhere($sql);
                                $ocupacion = count($entradas);

                            ?>

                            '<?php if ($ocupacion == 0) {
                                    echo "0";
                                } else {
                                    echo $ocupacion;
                                } ?>' // Imprimimos el valor en JS

                        <?php
                                if (date($mod_date) < date($hasta_mod)) {
                                    echo ",";
                                } // Nos permite imprimir la coma que separa los valores hasta el último dato
                            }
                        ?>
                    ],
                    spanGaps: false,
                }

            ]
        };

        // Notice the scaleLabel at the same level as Ticks
        var options = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                        display: false,
                        labelString: 'Moola',
                        fontSize: 20
                    }
                }]
            }
        };

        // Chart declaration:
        var myBarChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });

        ctx.canvas.parentNode.style.height = '300px';
    </script>


    <?php } ?>


    <?php require __DIR__ . "/../../views/partials/fin-doc.part.php"; ?>