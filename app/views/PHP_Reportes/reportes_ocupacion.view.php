<?php include __DIR__ . "/../../views/partials/navbar.partial.php"; // Importación del Nav Lateral 
require __DIR__ . "/../../views/partials/inicio-doc.part.php"; ?>



<script src="../js/date-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>



</head>

<body id="page-top" class="fondo">




    <?php require __DIR__ . "/../../views/partials/topbar.partial.php"; // Barra Superior 
    ?>


    <div class="row alto">

        <?php navegacion_Lateral($pagina_actual = "reports"); ?>


        <div class="col-xl-10 col-lg-12 col-md-12 col-sm-12 col-12 mt-5">


            <div class="row mt-5">
                <div class="col col-12">
                    <h3 style="font-size: 40px;" class="display-3 blockquote">Reports of the Ocupation</h3>
                    <hr class="border-top border-dark">
                </div>
            </div>



            <div class="col col-12 bg-white mt-5 p-3" id="Rooms_Border">



                <div class="row p-3">





                    <div class="col col-12 mt-3">




                        <form style="background-color: #F8FBF8;" id="miForm" action="reportes_ocupacion" method="POST" class="border rounded border-dark">
                            <div class="form-group row p-3 ">



                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="desde">From</label>
                                    <input onchange="myFunction()" value="<?php echo $desde ?>" id="desde" name="desde" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12 mt-1">
                                    <label for="hasta">To</label>
                                    <input value="<?php echo $hasta ?>" id="hasta" name="hasta" type="date" class="form-control" required>
                                </div>

                                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-3 ml-3">
                                    <p><input style="margin-top: 35px;" type="submit" value="Search" class="btn btn-success btn-block"></p>
                                </div>

                            </div>


                        </form>
                    </div>
                </div>



                <div class="col col-12 ">
                    <?php if ($filtro_desactivado == TRUE) { ?>
                        <canvas id="myChart"> </canvas>
                    <?php } else {
                    ?>
                        <p class="font-weight-bold text-center p-3">Please select estimated dates</p>
                    <?php
                    } ?>

                </div>




            </div>







        </div>


    </div>



<?php if($_SERVER["REQUEST_METHOD"] == "POST"){ ?> 


    <script>
            var ctx = document.getElementById("myChart").getContext("2d");
            var myChart = new Chart(ctx, {



                type: "bar",
                data: {


                    labels: [
                        <?php
                        $date = date($desde); // Introducimos el valor del primer Input
                        $hasta_mod = date($hasta); // Introducimos el valor del segundo Input


                        $mod_date = strtotime($date . "+ 0 days"); // Aplicamos el formato e inicializamos la variable, ya que si no la iniciamos el for fallará
                        $hasta_mod = strtotime($hasta . "+ 0 days");
                        ?>
                        <?php for ($i = 0; date($mod_date) <= date($hasta_mod); $mod_date = strtotime($date . "+ $i days")) {
                            $i++; ?> '<?php $fecha = date("d/m/Y", $mod_date);
                                        echo $fecha; ?> '
                        <?php if (date($mod_date) < date($hasta_mod)) {
                                echo ",";
                            }
                        } ?>,
                    ],
                    datasets: [{
                        label: 'Calendario de Ocupación',
                        data: [

                            <?php
                            $limitador_color = 0;

                            $date = date($desde); // Introducimos el valor del primer Input
                            $hasta_mod = date($hasta); // Introducimos el valor del segundo Input


                            $mod_date = strtotime($date . "+ 0 days"); // Aplicamos el formato e inicializamos la variable, ya que si no la iniciamos el for fallará
                            $hasta_mod = strtotime($hasta . "+ 0 days");

                            for ($i = 0; date($mod_date) <= date($hasta_mod); $mod_date = strtotime($date . "+ $i days")) {
                                $limitador_color++;
                                $i++;

                                $date_2 = date("Y-m-d", $mod_date); // Registro la Variable para la consulta

                             


                                $sql = "WHERE Estado = 1 AND Fecha_Entrada <= '$date_2' AND Fecha_Salida > '$date_2'";
                                

                                
                                    $ocupacion = $reservasRepository->findallWithWhere($sql); // No aplicamos Try - Catch, ya que en caso de error en Canvas no aparece, ya que en caso de Error, este no persiste, lo que imposibilita dar un mensaje a través del mismo
                                


                                

                            ?>

                                '<?php if (count($ocupacion) == 0) {
                                        echo "0";
                                    } else {
                                        echo count($ocupacion);
                                    } ?>' // Imprimimos el valor en JS

                            <?php
                                if (date($mod_date) < date($hasta_mod)) {
                                    echo ",";
                                } // Nos permite imprimir la coma que separa los valores hasta el último dato
                            }
                            ?>






                        ],
                        backgroundColor: [<?php for ($i = 0; $i <= $limitador_color; $i++) { ?> '<?php echo "rgb(0,0,255)" ?>'
                            <?php if (date($i) < date($limitador_color)) {
                                                    echo ",";
                                                }
                                            } ?>,
                        ]
                    }]
                },
                options: {
                    colors: ['red', 'green'],
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: "#FF0000"
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }

            });
            myChart.canvas.parentNode.style.height = '300px';
        </script>

        <?php } ?>




    <?php require __DIR__ . "/../../views/partials/fin-doc.part.php"; ?>