<?php require __DIR__ . "/../../views/partials/inicio-doc.part.php"; ?>

<script src="../plugins/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="../plugins/tinymce/js/tinymce/jquery.tinymce.min.js"></script>

<script src="../js/reservas.js"></script>


<link rel="stylesheet" href="../css/reservas.css">



<script>
    <?php if (empty($errores) == FALSE) { ?>
            (function() {
                $(function() {
                    $('#ventana-modal-errores').modal();
                });
            }());

    <?php } ?>
</script>



</head>

<body id="page-top" class="fondo">




    <?php require __DIR__ . "/../../views/partials/topbar.partial.php"; // Barra Superior 
    ?>
    <?php require __DIR__ . "/../../views/partials/Modal-Windows/reservation.modal.php"; // Contiene las Ventanas Modales 
    ?>




    <div class="row mt-5">



        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0 "></div>
        <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12 ">
            <h3 style="font-size: 40px;" class="display-3 blockquote">Update Reservation</h3>
            <hr class="border-top border-dark">
        </div>
        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0 "></div>


    </div>

    <div class="row">

        <div class="col col-1"></div>
        <div class="col col-10 bg-white rounded p-3">
            <form action="reservas_actualizadas?volver=<?= $_GET["volver"] ?>&&Idreserva=<?= $_GET["Idreserva"] ?>" method="POST" enctype="multipart/form-data">

                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote ">Informacion de la Reserva</h3>
                    </div>


                </div>

                <div class="form-row mt-3">

                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">NoReserva :</label>
                        <input type="text" class="form-control" id="numero_reserva" name="numero_reserva" value="<?= $reserva[0]->getNumero_Reserva() ?>" required>
                    </div>


                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Book day :</label>
                        <?php




                        $fecha_actual = date("d-m-Y");
                        ?>

                        <input name="book_day" type="date" class="form-control" id="fecha_registro" value="<?= $reserva[0]->getBook_Day() ?>" required>
                    </div>
                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Check in :</label>
                        <input id="fecha_entrada" onchange="myFunction()" name="fecha_entrada" type="date" class="form-control" value="<?= $reserva[0]->getFecha_Entrada() ?>" required>
                    </div>
                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Check out :</label>
                        <input id="fecha_salida" name="fecha_salida" type="date" class="form-control" value="<?= $reserva[0]->getFecha_Salida() ?>" required>
                    </div>



                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Number of adults :</label>
                        <div class="def-number-input number-input safari_only">

                            <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                            <input name="numero_adultos" class="quantity" min="1" name="quantity" type="number" value="<?= $reserva[0]->getNumero_Adultos() ?>" required>
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                        </div>

                    </div>

                    <div class="form-group col-xl-2 col-lg-4 col-md-6 col-sm-6 col-12">
                        <label for="email">Number of Children :</label>
                        <div class="def-number-input number-input safari_only">

                            <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                            <input name="numero_ninos" class="quantity" min="0" name="quantity" type="number" value="<?= $reserva[0]->getNumero_Ninios() ?>">
                            <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                        </div>

                    </div>






                </div>


                <div class="form-row">

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Travel Agent :</label>



                        <select name="operador" id="inputState" class="form-control">

                            <?php foreach ($operadores as $operador) : ?>

                                <option <?php
                                        if ($operador->getId() == $reserva[0]->getOperador()) { // MANTIENE LA CATEGORIA
                                            echo "selected";
                                        }
                                        ?> value="<?= $operador->getId() ?>"><?= $operador->getNombre(); ?></option>

                            <?php endforeach; ?>

                        </select>
                    </div>


                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Regime type :</label>

                        <select name="regimen" id="inputState" class="form-control">
                            <option <?php if ($reserva[0]->getPension() == "Only Bed") {
                                        echo "selected";
                                    } ?> value="Only Bed">Only Bed</option>
                            <option <?php if ($reserva[0]->getPension() == "Breakfast") {
                                        echo "selected";
                                    } ?> value="Breakfast">Breakfast</option>
                            <option <?php if ($reserva[0]->getPension() == "Half Board") {
                                        echo "selected";
                                    } ?> value="Half Board">Half Board</option>
                            <option <?php if ($reserva[0]->getPension() == "Full Board") {
                                        echo "selected";
                                    } ?> value="Full Board">Full Board</option>
                        </select>
                    </div>

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Pets :</label>
                        <input name="mascota" min="0" type="number" class="form-control" id="numero_reserva" value="<?= $reserva[0]->getMascota() ?>" required>
                    </div>



                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Apartament :</label>

                        <select name="apartamento" id="inputState" class="form-control">
                            <option value="0">Not Assigned</option>
                            <?php foreach ($apartamentos as $apartamento) : ?>

                                <option <?php
                                        if ($apartamento->getN_Habitacion() == $reserva[0]->getNumero_Apartamento()) { // MANTIENE LA CATEGORIA
                                            echo "selected";
                                        } ?> value="<?= $apartamento->getN_Habitacion() ?>"><?= $apartamento->getN_Habitacion(); ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote ">Datos Personales</h3>
                    </div>


                </div>

                <div class="form-row mt-3">

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Name :</label>
                        <input name="nombre" type="text" class="form-control" id="nane" value="<?= $reserva[0]->getNombre() ?>" required>
                    </div>

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Surname :</label>
                        <input name="apellidos" type="text" class="form-control" id="surname" value="<?= $reserva[0]->getApellidos() ?>" required>
                    </div>

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Phone :</label>
                        <input name="telefono" placeholder="Unknow" type="text" class="form-control" id="phone" value="<?= $reserva[0]->getTelefono() ?>">
                    </div>

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">




                        <label for="email">Country :</label>
                        <select name="country" id="inputState" class="form-control">
                            <?php foreach ($paises as $pais) : ?>

                                <option <?php
                                        if ($pais->getId() == $reserva[0]->getPais()) { // MANTIENE LA CATEGORIA
                                            echo "selected";
                                        } ?> value="<?= $pais->getId() ?>"><?= $pais->getNombre(); ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>


                </div>




                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote ">Payment</h3>
                    </div>

                </div>



                <div class="form-row mt-3">

                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Payment made :</label>
                        <input type="text" class="form-control" name="prepago" placeholder="0.00€" value="<?= $reserva[0]->getPrepago() ?>" required>
                    </div>




                    <div class="form-group col-xl-2 col-lg-3 col-md-3 col-sm-6 col-12">
                        <label for="email">Total price :</label>
                        <input type="text" class="form-control" name="precio_total" placeholder="0.00€" value="<?= $reserva[0]->getPrecio_Total() ?>" required>
                    </div>


                    <div class="col-xl-4 col-lg-3 col-md-0 col-sm-0 col-0"></div>

                    <div class="form-group col-xl-2 col-lg-6 col-md-6 col-sm-6 col-6 text-right">
                        <label style="margin-top: 42px;" for="email">Cancelled Reservation :</label>
                    </div>

                    <div class="form-group col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2">
                        <label class="chkbx" style="margin-top: 40px;">
                            <input <?php
                                    if ($reserva[0]->getEstado() == 0) { // Comprobación del campo Checked que me indica si se encuentra seleccionado o no en la consulta
                                        echo "checked";
                                    } ?> name="cancelada" type="checkbox">
                            <span class="x "></span>
                        </label>
                    </div>



                </div>





                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote ">Comments</h3>
                    </div>

                </div>



                <div class="form-row mt-3">

                    <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <textarea name="comentario" id="comentario" cols="30" rows="8"><?= $reserva[0]->getComentario() ?></textarea>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-0 col-sm-0 col-0">

                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 mt-5">
                        <button type="submit" class="btn btn-success btn-block">Update</button>
                        <a href="../<?php echo $_GET["volver"]; ?>" class="btn btn-dark btn-block">Cancel</a>
                    </div>

                    <div class="col col-1"></div>

                </div>


                <div class="form-row border-bottom mt-2">
                    <div class="col col-12">
                        <h3 style="font-size: 20px;" class="display-3 blockquote ">Logs</h3>
                    </div>

                </div>



                <div class="form-row mt-3">


                    <?php // Obtener el nombre de los usuarios en base a su DNI



                    ?>

                    <div class="form-group col col-7">
                        <h6>User who created the Reservation: <abbr class="lead"><?php echo $usuario_creado[0]->getNombre() ?></abbr></h6>
                        <h6>User who has made the last modification: <abbr class="lead"><?php echo $usuario_modifica[0]->getNombre() ?></abbr></h6>

                    </div>

                    <?php

                    if ($reserva[0]->getNombre_Archivo() == NULL) {
                    ?>
                        <div class="form-group col col-5">
                            <input class="form-control-file" name="imagen" type="file">
                        </div>
                    <?php
                    } else {
                    ?>
                        <a download href="files_reservation/<?= $reserva[0]->getNombre_Archivo()?>">Descargar Documento Asociado</a>

                    <?php
                    }
                    ?>








                </div>






            </form>
        </div>
        <div class="col col-1"></div>
    </div>





    <?php require __DIR__ . "/../../views/partials/fin-doc.part.php"; ?>