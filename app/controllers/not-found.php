<?php

// Página para indicar mantenimiento

if (isset($_SESSION["usuario"])) {

    require __DIR__ . "/../views/not-found.view.php";


} else {
    header("Location: /proyecto-dsw/");
}
