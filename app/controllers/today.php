<?php


if (isset($_SESSION["usuario"])) {



    $filtro_desactivado = NULL;



    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        $fecha = htmlspecialchars($_POST["fecha"]);
    } else {
        $fecha = date("Y-m-d"); // En el caso que el usuario no especifique fecha, este será HOY
    }


    $reservasRepository = new ReservaRepository();


    require __DIR__ . "/../views/today.view.php";
} else {
    header("Location: /proyecto-dsw/");
}
