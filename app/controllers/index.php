<?php

        use Monolog\Logger;
        use Monolog\Handler\StreamHandler;


try{


    if($_SERVER["REQUEST_METHOD"]==="POST"){



        $log = new Logger('Sesión');
        $log->pushHandler(new StreamHandler('logs/sesiones.log', Logger::WARNING));

   

        $usuariorepository = new UsuarioRepository(); 

        
        $nombre = htmlspecialchars($_POST["usuario"]); // Obtenemos el nombre de Usuario
        $contrasena = htmlspecialchars($_POST["password"]); // Obtenemos la contraseña

        $usuario = $usuariorepository->findUser($nombre,$contrasena); 


        if(empty($usuario) === FALSE){

            $_SESSION["usuario"] = $nombre;

            $log->warning('El usuario ' . $_SESSION["usuario"] . " ha iniciado sesión - " . date('l jS \of F Y h:i:s A'));

            $newURL = "home";
            header('Location: '.$newURL);
        }
        

    }

   

}


catch (AppException $appException) {


    $errores [] = $appException->getMessage();

} 

catch (NotFoundException $NotFoundExeption) {


    $errores [] = $NotFoundExeption->getMessage();

} 

catch (QueryException $QueryException) {


    $errores [] = $QueryException->getMessage();

} 

catch (PDOException $PDOException) {


    $errores [] = "Error en el SCRIPT";

} 



require __DIR__ . "/../views/index.view.php";
