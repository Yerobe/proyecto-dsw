<?php


if(isset($_SESSION["usuario"])){


try {

    
    $OperadoresRepository = new OperadorRepository();
    $operadores = $OperadoresRepository->findAllwithEstatus(); // Buscamos todos los Operadores Activados

    $apartamentosRepository = new ApartamentoRepository();
    $apartamentos = $apartamentosRepository->findAll(); // Buscamotos todos los apartamentos

    $paisRepository = new PaisRepository();
    $paises = $paisRepository->findAll(); // Buscamos todos los paises


    $reservasRepository = new ReservaRepository();





    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        /* Recogemos los diferentes campos */


        if (isset($_REQUEST["cancelada"])) { // Definimos el Estado de la Reserva [Cancelada = 0] - [Operativa = 1]
            $cancelada = "0"; // Por ello, si existe Cancelada, este es 1, y lo ponemos a 0 en la BD
        } else {
            $cancelada = "1";
        }


        /* Obtenemos el NIF del Usuario mediante el Nombre dado por la Sesión */

        $nombre = $_SESSION["usuario"]; // Identificamos al Usuario que realiza la inserción

        $usuariorepository = new UsuarioRepository();
        $usuario = $usuariorepository->findUserbyName($nombre);

        $usuario_nuevo = $usuario[0]->getNif(); // Obtenemos el DNI del Usuario que mantiene la Sesión, ya que esta, es la foránea de la tabla Usuario



        $prepago = htmlspecialchars($_POST["prepago"]);
        $precio_total = htmlspecialchars($_POST["precio_total"]);

        if ($prepago > $precio_total) { // Un cliente no puede pagar mas de lo que vale la reserva
            $prepago = 0; // En este caso, lo mandaremos a 0 - UnPaid
        }



        $prepago = str_replace(',', '.', $prepago); // En el caso de que se escriba 5,14, lo sustituimos por 5.14 ya que la BBDD no coge 5,14
        $precio_total = str_replace(',', '.', $precio_total); // En el caso de que se escriba 5,14, lo sustituimos por 5.14 ya que la BBDD no coge 5,14


        /* Recogida de los datos del Formulario */

        $numero_reserva = htmlspecialchars($_POST["numero_reserva"]);
        $book_day = htmlspecialchars($_POST["book_day"]);
        $fecha_entrada = htmlspecialchars($_POST["fecha_entrada"]);
        $fecha_salida = htmlspecialchars($_POST["fecha_salida"]);
        $numero_adultos = htmlspecialchars($_POST["numero_adultos"]);
        $numero_ninos = htmlspecialchars($_POST["numero_ninos"]);
        $operador = htmlspecialchars($_POST["operador"]);
        $regimen = htmlspecialchars($_POST["regimen"]);
        $mascota = htmlspecialchars($_POST["mascota"]);
        $apartamento = htmlspecialchars($_POST["apartamento"]);
        $nombre = ucwords(strtolower(htmlspecialchars($_POST["nombre"]))); // Si el nombre el ALFRedo, este se convertirá en Alfredo
        $apellidos = ucwords(strtolower(htmlspecialchars($_POST["apellidos"])));  // Si el nombre el ALFRedo, este se convertirá en Alfredo
        $comentario = htmlspecialchars($_POST["comentario"]);
        $country = htmlspecialchars($_POST["country"]);
        $telefono = htmlspecialchars($_POST["telefono"]);
        $usuario_creado = $usuario_nuevo;
        $usuario_modificado = $usuario_nuevo;
        $Nombre_Archivo = NULL; // Lo especificamos en NULL, ya que no permitiré que en el INSERT, se inserten ficheros

        $fecha_registro = date("Y-m-d"); // Obtenemos de forma automática el día de Registro [El Usuario no puede manipular este]

  

        if(empty($telefono)){  // El caso en el que la Reserva no cuente con número de Teléfono
            $telefono = NULL;
        }

        if($apartamento === "0"){ // No podemos definir null como value, ya que es de tipo String, por lo que interpretamos 0 como Null y realizamos la conversión
            $apartamento = NULL;
        }
        

        
        $reserva = new Reserva($numero_reserva,$operador,$book_day,$fecha_entrada,$fecha_salida,$fecha_registro,$regimen,$nombre,$apellidos,$numero_adultos,$numero_ninos,$country,$mascota,$prepago,$precio_total,$comentario,
        $cancelada,$telefono,$apartamento,$usuario_creado,$usuario_modificado,$Nombre_Archivo);

        $reservasRepository->save_reserva($reserva); // Realiza el INSERT de la Reserva
     

        if($_GET["volver"] == "home"){ // Página a la que debo regresar al realizar la Inserción
            header('Location: ../home');
        }
        

   
        


    }


}catch (QueryException $QueryException) {


    $errores [] = $QueryException->getMessage();

} 


catch (NotFoundException $NotFoundExeption) {


    $errores[] = $NotFoundExeption->getMessage();
}








require __DIR__ . "/../../views/PHP_Reservas/reservas_nuevas.view.php";



}else{
    header("Location: /proyecto-dsw/");
}