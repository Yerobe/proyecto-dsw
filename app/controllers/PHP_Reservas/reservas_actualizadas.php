<?php


if (isset($_SESSION["usuario"])) {


    try {


        $numeroid = htmlspecialchars($_GET["Idreserva"]); // Obtenemos el Identificador por GET

        $condicional = "WHERE Id = $numeroid"; // Crearemos el condicionel que se añadirá al SQL


        $OperadoresRepository = new OperadorRepository(); 
        $operadores = $OperadoresRepository->findAllwithEstatus(); // Obtenemos los Operadores Activos

        $apartamentosRepository = new ApartamentoRepository();
        $apartamentos = $apartamentosRepository->findAll(); // Obtenemos todos los Apartamentos

        $paisRepository = new PaisRepository();
        $paises = $paisRepository->findAll(); // Obtenemos todos los Países


        $reservasRepository = new ReservaRepository();
        $reserva = $reservasRepository->findallWithWhere($condicional); // Obtenemos la reserva específica

        $usuariorepository = new UsuarioRepository();
        $usuario_creado = $usuariorepository->findUserByNIF($reserva[0]->getUsuario_Creado()); // Obtenemos el Usuario que ha creado la reserva
        $usuario_modifica = $usuariorepository->findUserByNIF($reserva[0]->getUsuario_Modifica()); 



        if ($_SERVER["REQUEST_METHOD"] == "POST") {



            if (!$_FILES) { // Si no se introduce archivo, establecemos NULL
                $Nombre_Archivo = "NULL";
            } else {

                $tiposAceptados = ["application/pdf"]; // Solamente formato PDF
                $imagen = new File("imagen", $tiposAceptados); // Crearemos el objeto

                $imagen->saveUploadFile(Reserva::RUTA_IMAGENES); // Lo guardamos en el destino indicado
                $Nombre_Archivo = $imagen->getFileName(); // Obtenemos el Nombre de este 'archivo.pdf'
            }



            /* Recogemos los diferentes campos */


            if (isset($_REQUEST["cancelada"])) { // Definimos el Estado de la Reserva [Cancelada = 0] - [Operativa = 1]
                $cancelada = "0"; // Por ello, si existe Cancelada, este es 1, y lo ponemos a 0 en la BD
            } else {
                $cancelada = "1";
            }


            /* Obtenemos el NIF del Usuario mediante el Nombre dado por la Sesión */

            $nombre = $_SESSION["usuario"]; // Identificamos al Usuario que realiza la inserción


            $usuario = $usuariorepository->findUserbyName($nombre);

            $usuario_nuevo = $usuario[0]->getNif(); // Obtenemos el DNI del Usuario que mantiene la Sesión, ya que esta, es la foránea de la tabla Usuario



            $prepago = htmlspecialchars($_POST["prepago"]);
            $precio_total = htmlspecialchars($_POST["precio_total"]);

            if ($prepago > $precio_total) { // Un cliente no puede pagar mas de lo que vale la reserva
                $prepago = 0; // En este caso, lo mandaremos a 0 - UnPaid
            }



            $prepago = str_replace(',', '.', $prepago); // En el caso de que se escriba 5,14, lo sustituimos por 5.14 ya que la BBDD no coge 5,14
            $precio_total = str_replace(',', '.', $precio_total); // En el caso de que se escriba 5,14, lo sustituimos por 5.14 ya que la BBDD no coge 5,14


            /* Recogida de los datos del Formulario */

            $numero_reserva = htmlspecialchars($_POST["numero_reserva"]);
            $book_day = htmlspecialchars($_POST["book_day"]);
            $fecha_entrada = htmlspecialchars($_POST["fecha_entrada"]);
            $fecha_salida = htmlspecialchars($_POST["fecha_salida"]);
            $numero_adultos = htmlspecialchars($_POST["numero_adultos"]);
            $numero_ninos = htmlspecialchars($_POST["numero_ninos"]);
            $operador = htmlspecialchars($_POST["operador"]);
            $regimen = htmlspecialchars($_POST["regimen"]);
            $mascota = htmlspecialchars($_POST["mascota"]);
            $apartamento = htmlspecialchars($_POST["apartamento"]);
            $nombre = ucwords(strtolower(htmlspecialchars($_POST["nombre"]))); // Si el nombre el ALFRedo, este se convertirá en Alfredo
            $apellidos = ucwords(strtolower(htmlspecialchars($_POST["apellidos"])));  // Si el nombre el ALFRedo, este se convertirá en Alfredo
            $comentario = htmlspecialchars($_POST["comentario"]);
            $country = htmlspecialchars($_POST["country"]);
            $telefono = htmlspecialchars($_POST["telefono"]);
            $usuario_creado_nuevo = $reserva[0]->getUsuario_Creado(); // El usuario creado no se modificará
            $usuario_modificado = $usuario_nuevo;


            $fecha_registro = $reserva[0]->getFecha_Creado();


            $reserva_nueva = new Reserva(
                $numero_reserva,
                $operador,
                $book_day,
                $fecha_entrada,
                $fecha_salida,
                $fecha_registro,
                $regimen,
                $nombre,
                $apellidos,
                $numero_adultos,
                $numero_ninos,
                $country,
                $mascota,
                $prepago,
                $precio_total,
                $comentario,
                $cancelada,
                $telefono,
                $apartamento,
                $usuario_creado_nuevo,
                $usuario_modificado,
                $Nombre_Archivo
            );

            $reservasRepository->update_reserva($reserva_nueva, $numeroid); // Realiz a el INSERT de la Reserva



            if (empty($errores)) {
                header('Location: ../' . $_GET["volver"]);
            }
        }
    } catch (QueryException $QueryException) {


        $errores[] = $QueryException->getMessage();
    } catch (NotFoundException $NotFoundExeption) {


        $errores[] = $NotFoundExeption->getMessage();
    } catch (PDOException $PDOException) {


        $errores[] = "Error en el SCRIPT de la BBDD";
    } catch (FileException $FileException) {


        $errores[] = $FileException->getMessage();
    }



    require __DIR__ . "/../../views/PHP_Reservas/reservas_actualizadas.view.php";
} else {
    header("Location: /proyecto-dsw/");
}
