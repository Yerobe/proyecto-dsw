<?php


if (isset($_SESSION["usuario"])) {



    if (!isset($_GET["pagina"])) { // Controlador de Página
        $_GET["pagina"] = 0;
    }


    try {

        $OperadoresRepository = new OperadorRepository();
        $operadores = $OperadoresRepository->findAll();

        $apartamentosRepository = new ApartamentoRepository();
        $apartamentos = $apartamentosRepository->findAll();
    } catch (NotFoundException $NotFoundExeption) {

        $errores[] = $NotFoundExeption->getMessage();
    }





    if ($_SERVER["REQUEST_METHOD"] == "POST") {


        $where_active = FALSE;


        /* FILTRO DE BÚSQUEDA DEL NÚMERO DE RESERVA */

        if (isset($_POST["numero_reserva"]) && !empty($_POST["numero_reserva"])) {
            $numero = trim(strtolower($_POST["numero_reserva"]));
            $filtro_numero = "WHERE (Numero_Reserva LIKE '%$numero%' OR Nombre LIKE '%$numero%' OR Apellidos LIKE '%$numero%')";
            $where_active = TRUE;
        } else {
            $filtro_numero = "";
        }


        /* FILTRO DE BÚSQUEDA DEL BOOKDAY */

        if (isset($_POST["bookday"]) && !empty($_POST["bookday"])) {
            $bookday = $_POST["bookday"];
            if ($where_active == FALSE) {
                $filtro_bookday = "WHERE Book_Day = '$bookday'";
                $where_active = TRUE;
            } else {
                $filtro_bookday = "AND Book_Day = '$bookday'";
            }
        } else {
            $filtro_bookday = "";
            $bookday = "";
        }



        /* FILTRO DE BÚSQUEDA DEL CHECKIN */

        if (isset($_POST["fecha_entrada"]) && !empty($_POST["fecha_entrada"])) {
            $checkin = $_POST["fecha_entrada"];
            if ($where_active == FALSE) {
                if (isset($_POST["fecha_salida"]) && !empty($_POST["fecha_salida"])) {
                    $filtro_checkin = "WHERE Fecha_Entrada >= '$checkin'"; // En el caso de que se desee hacer la búsqueda desde una fecha determinada hacia adelante
                    $where_active = TRUE;
                } else {
                    $filtro_checkin = "WHERE Fecha_Entrada = '$checkin'"; // En el caso de que se desee una fecha de entrada exacta
                    $where_active = TRUE;
                }
            } else {
                if (isset($_POST["fecha_salida"]) && !empty($_POST["fecha_salida"])) {
                    $filtro_checkin = "AND Fecha_Entrada >= '$checkin'"; // En el caso de que se desee hacer la búsqueda desde una fecha determinada hacia adelante
                } else {
                    $filtro_checkin = "AND Fecha_Entrada = '$checkin'";  // En el caso de que se desee una fecha de entrada exacta
                }
            }
        } else {
            $filtro_checkin = "";
            $checkin = "";
        }




        /* FILTRO DE BÚSQUEDA DEL CHECKOUT */

        if (isset($_POST["fecha_salida"]) && !empty($_POST["fecha_salida"])) {
            $checkout = $_POST["fecha_salida"];
            if ($where_active == FALSE) {
                if (isset($_POST["fecha_entrada"]) && !empty($_POST["fecha_entrada"])) {
                    $filtro_checkout = "WHERE Fecha_Salida <= '$checkout'";
                    $where_active = TRUE;
                } else {
                    $filtro_checkout = "WHERE Fecha_Salida = '$checkout'";
                    $where_active = TRUE;
                }
            } else {
                if (isset($_POST["fecha_entrada"]) && trim($_POST["fecha_entrada"]) != "") {
                    $filtro_checkout = "AND Fecha_Salida <= '$checkout'";
                } else {
                    $filtro_checkout = "AND Fecha_Salida = '$checkout'";
                }
            }
        } else {
            $filtro_checkout = "";
            $checkout = "";
        }





        /* FILTRO DE BÚSQUEDA DEL TRAVEL-AGENT */

        if (isset($_POST["operador"]) && $_POST["operador"] != 'ALL') {
            $operador = htmlspecialchars($_POST["operador"]);
            if ($where_active == FALSE) {
                $filtro_operador = "WHERE Operador = '$operador'";
                $where_active = TRUE;
            } else {
                $filtro_operador = "AND Operador = '$operador'";
            }
        } else {
            $filtro_operador = "";
            $operador = "";
        }






        /* FILTRO DE BÚSQUEDA DEL RÉGIMEN */

        if (isset($_POST["regimen"]) && $_POST["regimen"] != 'ALL') {
            $regimen = htmlspecialchars($_POST["regimen"]);
            if ($where_active == FALSE) {
                $filtro_regimen = "WHERE Pension = '$regimen'";
                $where_active = TRUE;
            } else {
                $filtro_regimen = "AND Pension = '$regimen'";
            }
        } else {
            $filtro_regimen = "";
            $regimen = "";
        }





        /* FILTRO DE BÚSQUEDA DE LOS APARTAMENTOS */

        if (isset($_POST["apartamento"]) && $_POST["apartamento"] != 'ALL') {
            $apartamento = htmlspecialchars($_POST["apartamento"]);
            if ($where_active == FALSE) {
                $filtro_apartamento = "WHERE Numero_Apartamento = '$apartamento'";
                $where_active = TRUE;
            } else {
                $filtro_apartamento = "AND Numero_Apartamento = '$apartamento'";
            }
        } else {
            $filtro_apartamento = "";
            $apartamento = "";
        }



        /* FILTRO DE BÚSQUEDA DE LAS RESERVAS EN ESTADO DEL PAGO */


        if (isset($_POST["payment_status"]) && $_POST["payment_status"] != 'ALL') { // Realiza la búsqueda si se especifica solamente un tipo de régimen, si no se buscarán todos
            $pago = htmlspecialchars($_POST["payment_status"]);
            if ($where_active == FALSE) {
                if ($_POST["payment_status"] == 'Pagado') {
                    $filtro_pago = "WHERE Prepago = Precio_Total";
                    $where_active = TRUE;
                } else {
                    $filtro_pago = "WHERE Prepago < Precio_Total";
                    $where_active = TRUE;
                }
            } else {
                if ($_POST["payment_status"] == 'Pagado') {
                    $filtro_pago = "AND Prepago = Precio_Total";
                } else {
                    $filtro_pago = "AND Prepago < Precio_Total";
                }
            }
        } else {
            $filtro_pago = "";
            $pago = "";
        }






        /* FILTRO DE BÚSQUEDA DE LAS RESERVAS SEGÚN SU ESTADO */

        if (isset($_POST["estado"]) && trim($_POST["estado"]) != 'ALL') { // Realiza la búsqueda si se especifica solamente un tipo de régimen, si no se buscarán todos
            $estado = $_POST["estado"];
            if ($where_active == FALSE) {
                $filtro_estado = "WHERE Estado = '$estado'";
                $where_active = TRUE;
            } else {
                $filtro_estado = "AND Estado = '$estado'";
            }
        } else {
            $filtro_estado = "";
            $estado = "";
        }
    }




    /* CON ESTA FUNCIÓN PERMITO QUE NO ME APAREZCAN TODAS LAS RESERVAS EN EL CASO DE QUE NO HAYA FILTRO SELECCIONADO */

    if (empty($filtro_numero) && empty($filtro_bookday) && empty($filtro_checkin) && empty($filtro_checkout) && empty($filtro_operador) && empty($filtro_regimen) && empty($filtro_apartamento) && empty($filtro_pago) && empty($filtro_estado) && $_GET["pagina"] == 0) {
        $filtro_desactivado = NULL;
        $_SESSION["consulta"] = "SELECT * FROM reservas";
    } else {
        $filtro_desactivado = TRUE;
        if ($_GET["pagina"] >= 1) {
        } else {
            $_SESSION["consulta"] = "SELECT * FROM reservas $filtro_numero $filtro_bookday $filtro_checkin $filtro_checkout $filtro_operador $filtro_regimen $filtro_apartamento $filtro_pago $filtro_estado ORDER BY Fecha_Entrada ASC";
        }
    }

    if ($_GET["pagina"] > 1) {
        $filtro_desactivado = TRUE;
    }


    $ssql = $_SESSION["consulta"]; // Mantenemos la búsqueda en una Sesión


    try {
        $reservasRepository = new ReservaRepository();
        $reservaswithFilter = $reservasRepository->findReservationWithFilter($ssql);
    } catch (PDOException $PDOException) {

        $errores[] = "Error en el SCRIPT de la BBDD";
    } catch (NotFoundException $NotFoundExeption) {

        $errores[] = $NotFoundExeption->getMessage();
    }




    if (isset($reservaswithFilter)) { // Configurador de cuantas reservas se mostrarán por página

        //Limito la busqueda

        $TAMANO_PAGINA = 150;

        $pagina = $_GET["pagina"];

        if (!$pagina) {
            $inicio = 0;
            $pagina = 1;
        } else {
            $inicio = ($pagina - 1) * $TAMANO_PAGINA;
        }

        $num_total_registros = count($reservaswithFilter);
        $total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);
    }










    require __DIR__ . "/../views/seeker.view.php";



    /* Los siguiente Campos me permiten establecer los Efectos JS, en el caso de existencia */


    if (isset($_POST["bookday"]) && trim($_POST["bookday"]) != "") {
?>
        <script>
            var bookday = document.getElementById('bookday'); //se define la variable "el" igual a nuestro div
            bookday.style.display = "none";
        </script>

    <?php
    }


    if (isset($_POST["fecha_entrada"]) && trim($_POST["fecha_entrada"]) != "") {
    ?>
        <script>
            var checkin = document.getElementById('checkin'); //se define la variable "el" igual a nuestro div
            checkin.style.display = "none";
        </script>

    <?php
    }


    if (isset($_POST["fecha_salida"]) && trim($_POST["fecha_salida"]) != "") {
    ?>
        <script>
            var checkout = document.getElementById('checkout'); //se define la variable "el" igual a nuestro div
            checkout.style.display = "none";
        </script>

    <?php
    }



    if (isset($_POST["operador"]) && trim($_POST["operador"]) != "ALL") {
    ?>
        <script>
            var operador = document.getElementById('companie'); //se define la variable "el" igual a nuestro div
            operador.style.display = "none";
        </script>

    <?php
    }


    if (isset($_POST["regimen"]) && trim($_POST["regimen"]) != "ALL") {
    ?>
        <script>
            var regimen = document.getElementById('regimen'); //se define la variable "el" igual a nuestro div
            regimen.style.display = "none";
        </script>

    <?php
    }



    if (isset($_POST["apartamento"]) && trim($_POST["apartamento"]) != "ALL") {
    ?>
        <script>
            var apartamento = document.getElementById('apartamento'); //se define la variable "el" igual a nuestro div
            apartamento.style.display = "none";
        </script>

    <?php
    }



    if (isset($_POST["payment_status"]) && trim($_POST["payment_status"]) != "ALL") {
    ?>
        <script>
            var pago = document.getElementById('pago'); //se define la variable "el" igual a nuestro div
            pago.style.display = "none";
        </script>

    <?php
    }




    if (isset($_POST["estado"]) && trim($_POST["estado"]) != "ALL") {
    ?>
        <script>
            var estado = document.getElementById('estado'); //se define la variable "el" igual a nuestro div
            estado.style.display = "none";
        </script>

<?php
    }
} else {
    header("Location: /proyecto-dsw/");
}
