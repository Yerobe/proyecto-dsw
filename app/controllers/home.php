<?php

if(isset($_SESSION["usuario"])){

include __DIR__ . "../../views/partials/navbar.partial.php"; // Importación del Nav Lateral


$dia_actual = date('Y/m/d'); // Obtenemos el día de Hoy


if(!isset($_GET["dia"])){ // Me permite determinar el día que nos encontramos [Yesterday - Today - Tomorrow]
    $_GET["dia"] = "";
    $fecha_busqueda = $dia_actual;
}elseif($_GET["dia"] == "yesterday"){
    $fecha_busqueda = strtotime('-1 days', strtotime($dia_actual)); // Obtenemos el Día de Ayer
    $fecha_busqueda = date('Y/m/d', $fecha_busqueda);
}elseif($_GET["dia"] == "tomorrow"){
    $fecha_busqueda = strtotime('+1 days', strtotime($dia_actual)); // Obtenemos el Día de Mañana
    $fecha_busqueda = date('Y/m/d', $fecha_busqueda);
}


try{
    $reservasTodayrepository = new ReservaRepository();
    $reservas_checkin = $reservasTodayrepository->finReservationCheckInforDay($fecha_busqueda); // Busqueda de las entradas según el día
    $reservas_checkout = $reservasTodayrepository->finReservationCheckOutforDay($fecha_busqueda); // Busqueda de las salidas según el día
    
}catch (PDOException $PDOException) {

    $errores[] = "Error en el SCRIPT de la BBDD";
}



require __DIR__ . "/../views/home.view.php";

}else{
    header("Location: /proyecto-dsw/");
}
