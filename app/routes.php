<?php 

// Mantiene los redireccionamientos a las Rutas Amigables


return [

 "proyecto-dsw" => "app/controllers/index.php",

 "proyecto-dsw/home" => "app/controllers/home.php",

 "proyecto-dsw/booking" => "app/controllers/booking.php",

 "proyecto-dsw/seeker" => "app/controllers/seeker.php",

 "proyecto-dsw/restaurant" => "app/controllers/restaurant.php",

 "proyecto-dsw/today" => "app/controllers/today.php",

 "proyecto-dsw/logout" => "app/controllers/logout.php",

 "proyecto-dsw/not-found" => "app/controllers/not-found.php",




 "proyecto-dsw/PHP_Reservas/reservas_nuevas" => "app/controllers/PHP_Reservas/reservas_nuevas.php",

 "proyecto-dsw/PHP_Reservas/reservas_actualizadas" => "app/controllers/PHP_Reservas/reservas_actualizadas.php",

 "proyecto-dsw/PHP_Reportes/reportes_ocupacion" => "app/controllers/PHP_Reportes/reportes_ocupacion.php",

 "proyecto-dsw/PHP_Reportes/reportes_checkin-checkout" => "app/controllers/PHP_Reportes/reportes_checkin-checkout.php"

]

?>