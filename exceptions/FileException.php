<?php

class FileException extends Exception // Este es una herencia de objetos

{

    public function __construct (string $message) {

        parent::__construct($message); // Este Operador de Resolución, nos permite acceder a elementos estáticos, constantes, y sobrescribir propiedades o métodos de una clase


    }

}

?>