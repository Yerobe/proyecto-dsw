<?php

class File
{



    private $file;

    private $fileName;


    public function __construct(string $fileName, array $arrTypes)
    {

        $this->file = $_FILES[$fileName];

        $this->fileName = "";


        if ($this->file["error"] !== UPLOAD_ERR_OK) { // UPLOAD_ERR_OK = Documento subido con éxito

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE: // Supera el Límite de tamaño especificado en php.ini
                    throw new FileException("Error en la limitación php.ini", 1);
                case UPLOAD_ERR_FORM_SIZE: // Supera el Límite de tamaño especificado en el Form de HTML
                    throw new FileException("Error en la limitación HTML", 1);
                case UPLOAD_ERR_PARTIAL: // El fichero fué subido parcialmente no de forma completa
                    throw new FileException("El archivo no ha podido ser subido de forma completa", 1);
                default:
                    throw new FileException("Error Desconocido", 1);

                    break;
            }
        }

        if (in_array($this->file["type"], $arrTypes) === false) {

            throw new FileException("El archivo subido no cumple el formato correcto", 1);
        }
    }



    public function saveUploadFile(string $rute)
    {


        if (is_uploaded_file($this->file["tmp_name"])) { //Comprueba que es un fichero subido mediante formulario

            if (is_file($rute . $this->file["name"])) { // Si el archivo ya es existente en la ruta se Renombrará

                $idUnico = time() . "_" . $this->file["name"]; // Concatenación del nombre
                $this->file["name"] = $idUnico; // Renombración del archivo
            }

            if (move_uploaded_file($this->file["tmp_name"], $rute . $this->file["name"])) {
                // Se ha realizado El Movimiento de forma Correcta
            } else {
                throw new FileException("No se ha podido mover el fichero al destino especificado", 1);
            }
        } else {
            throw new FileException("El archivo no se ha subido desde un formulario", 1);
        }
    }




    /**
     * Get the value of fileName
     */
    public function getFileName()
    {
        return $this->fileName . $this->file["name"];
    }
}
