<?php

require_once "database/IEntity.php";

class Pais implements IEntity  // Pais de SGBD
{

    private $id;
    private $iso;
    private $nombre;



    public function __construct($id = 0, $iso = NULL, $nombre = " ")
    {

        $this->id = $id;

        $this->iso = $iso;

        $this->nombre = $nombre;

    }


    public function toArray(): array

    {

        return [

            "id" => $this->getId(),

            "iso" => $this->getIso(),

            "nombre" => $this->getNombre()

        ];
    }



    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of iso
     */ 
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * Set the value of iso
     *
     * @return  self
     */ 
    public function setIso($iso)
    {
        $this->iso = $iso;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
    }
