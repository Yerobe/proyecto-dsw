<?php

require_once "database/IEntity.php";

class Apartamento implements IEntity
{

    private $N_Habitacion;
    private $Nivel;
    private $Tipo;
    private $Tipo_Baño;
    private $Cama;
    private $Mascotas;
    private $Discapacitados;
    private $Estado;



    public function __construct($N_Habitacion = 101, $Nivel = "Bajo", $Tipo= "Normal", $Tipo_Baño = "Bañera" , $Cama = "Individual", $Mascotas = 0, $Discapacitados = 0, $Estado = "Bloqueado")
    {

        $this->N_Habitacion = $N_Habitacion;

        $this->Nivel = $Nivel;

        $this->Tipo = $Tipo;

        $this->Tipo_Baño = $Tipo_Baño;

        $this->Cama = $Cama;

        $this->Mascotas = $Mascotas;

        $this->Discapacitados = $Discapacitados;

        $this->Estado = $Estado;
    }


    public function toArray(): array

    {

        return [

            "N_Habitacion" => $this->getN_Habitacion(),

            "Nivel" => $this->getNivel(),

            "Tipo" => $this->getTipo(),

            "Tipo_Baño" => $this->getTipo_Baño(),

            "Cama" => $this->getCama(),

            "Mascotas" => $this->getMascotas(),

            "Discapacitados" => $this->getDiscapacitados(),

            "Estado" => $this->getEstado()

        ];
    }



   

    /**
     * Get the value of N_Habitacion
     */ 
    public function getN_Habitacion()
    {
        return $this->N_Habitacion;
    }

    /**
     * Set the value of N_Habitacion
     *
     * @return  self
     */ 
    public function setN_Habitacion($N_Habitacion)
    {
        $this->N_Habitacion = $N_Habitacion;

        return $this;
    }

    /**
     * Get the value of Nivel
     */ 
    public function getNivel()
    {
        return $this->Nivel;
    }

    /**
     * Set the value of Nivel
     *
     * @return  self
     */ 
    public function setNivel($Nivel)
    {
        $this->Nivel = $Nivel;

        return $this;
    }

    /**
     * Get the value of Tipo
     */ 
    public function getTipo()
    {
        return $this->Tipo;
    }

    /**
     * Set the value of Tipo
     *
     * @return  self
     */ 
    public function setTipo($Tipo)
    {
        $this->Tipo = $Tipo;

        return $this;
    }

    /**
     * Get the value of Tipo_Baño
     */ 
    public function getTipo_Baño()
    {
        return $this->Tipo_Baño;
    }

    /**
     * Set the value of Tipo_Baño
     *
     * @return  self
     */ 
    public function setTipo_Baño($Tipo_Baño)
    {
        $this->Tipo_Baño = $Tipo_Baño;

        return $this;
    }

    /**
     * Get the value of Cama
     */ 
    public function getCama()
    {
        return $this->Cama;
    }

    /**
     * Set the value of Cama
     *
     * @return  self
     */ 
    public function setCama($Cama)
    {
        $this->Cama = $Cama;

        return $this;
    }

    /**
     * Get the value of Mascotas
     */ 
    public function getMascotas()
    {
        return $this->Mascotas;
    }

    /**
     * Set the value of Mascotas
     *
     * @return  self
     */ 
    public function setMascotas($Mascotas)
    {
        $this->Mascotas = $Mascotas;

        return $this;
    }

    /**
     * Get the value of Discapacitados
     */ 
    public function getDiscapacitados()
    {
        return $this->Discapacitados;
    }

    /**
     * Set the value of Discapacitados
     *
     * @return  self
     */ 
    public function setDiscapacitados($Discapacitados)
    {
        $this->Discapacitados = $Discapacitados;

        return $this;
    }

    /**
     * Get the value of Estado
     */ 
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Set the value of Estado
     *
     * @return  self
     */ 
    public function setEstado($Estado)
    {
        $this->Estado = $Estado;

        return $this;
    }
}
