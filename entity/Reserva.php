<?php

require_once "database/IEntity.php";

class Reserva implements IEntity  // Reserva de SGBD
{

    private $Id;
    private $Numero_Reserva;
    private $Operador;
    private $Book_Day;
    private $Fecha_Entrada;
    private $Fecha_Salida;
    private $Fecha_Creado;
    private $Pension;
    private $Nombre;
    private $Apellidos;
    private $Numero_Adultos;
    private $Numero_Ninios;
    private $Pais;
    private $Mascota;
    private $Prepago;
    private $Precio_Total;
    private $Comentario;
    private $Estado;
    private $Numero_Apartamento;
    private $Telefono;
    private $Usuario_Creado;
    private $Usuario_Modifica;
    private $Nombre_Archivo;


    const RUTA_IMAGENES = "files_reservation/"; // Ruta de archivos


    public function __construct($Numero_Reserva = "0", $Operador = 1, $Book_Day = "9999/12/30", $Fecha_Entrada  = "9999/12/30", $Fecha_Salida  = "9999/12/30", $Fecha_Creado = "9999/12/30", $Pension = "Only Bed", $Nombre = "No Tengo", $Apellidos = "No Tengo", 
    $Numero_Adultos = 0, $Numero_Ninios = 0, $Pais = 73, $Mascota = 0, $Prepago = 0, $Precio_Total = 0, $Comentario = "Vacio", $Estado = 0, $Telefono = NULL, $Numero_Apartamento = NULL,
    $Usuario_Creado = "", $Usuario_Modifica = "", $Nombre_Archivo = NULL){

    $this->Id = NULL;
    $this->Numero_Reserva = $Numero_Reserva;
    $this->Operador = $Operador;
    $this->Book_Day = $Book_Day;
    $this->Fecha_Entrada = $Fecha_Entrada;
    $this->Fecha_Salida = $Fecha_Salida;
    $this->Fecha_Creado = $Fecha_Creado;
    $this->Pension = $Pension;
    $this->Nombre = $Nombre;
    $this->Apellidos = $Apellidos;
    $this->Numero_Adultos = $Numero_Adultos;
    $this->Numero_Ninios = $Numero_Ninios;
    $this->Pais = $Pais;
    $this->Mascota = $Mascota;
    $this->Prepago = $Prepago;
    $this->Precio_Total = $Precio_Total;
    $this->Comentario = $Comentario;
    $this->Estado = $Estado;
    $this->Telefono = $Telefono;
    $this->Numero_Apartamento = $Numero_Apartamento;
    $this->Usuario_Creado = $Usuario_Creado;
    $this->Usuario_Modifica = $Usuario_Modifica;
    $this->Nombre_Archivo = $Nombre_Archivo;


    }


    public function toArray(): array

    {

        return [

            "Numero_Reserva" => $this->getNumero_Reserva(),
            "Operador" => $this->getOperador(),
            "Book_Day" => $this->getBook_Day(),
            "Fecha_Entrada" => $this->getFecha_Entrada(),
            "Fecha_Salida" => $this->getFecha_Salida(),
            "Fecha_Creado" => $this->getFecha_Creado(),
            "Pension" => $this->getPension(),
            "Nombre" => $this->getNombre(),
            "Apellidos" => $this->getApellidos(),
            "Numero_Adultos" => $this->getNumero_Adultos(),
            "Numero_Ninios" => $this->getNumero_Ninios(),
            "Pais" => $this->getPais(),
            "Mascota" => $this->getMascota(),
            "Prepago" => $this->getPrepago(),
            "Precio_Total" => $this->getPrecio_Total(),
            "Comentario" => $this->getComentario(),
            "Estado" => $this->getEstado(),
            "Telefono" => $this->getTelefono(),
            "Numero_Apartamento" => $this->getNumero_Apartamento(),
            "Usuario_Creado" => $this->getUsuario_Creado(),
            "Usuario_Modifica" => $this->getUsuario_Modifica(),
            "Nombre_Archivo" => $this->getNombre_Archivo()
        ];
    }



    public function getURLPortfolio(): string
    {
        return self::RUTA_IMAGENES . $this->getNombre();
    }




    /**
     * Get the value of Id
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of Id
     *
     * @return  self
     */ 
    public function setId($Id)
    {
        $this->Id = $Id;

        return $this;
    }

    /**
     * Get the value of Numero_Reserva
     */ 
    public function getNumero_Reserva()
    {
        return $this->Numero_Reserva;
    }

    /**
     * Set the value of Numero_Reserva
     *
     * @return  self
     */ 
    public function setNumero_Reserva($Numero_Reserva)
    {
        $this->Numero_Reserva = $Numero_Reserva;

        return $this;
    }

    /**
     * Get the value of Operador
     */ 
    public function getOperador()
    {
        return $this->Operador;
    }

    /**
     * Set the value of Operador
     *
     * @return  self
     */ 
    public function setOperador($Operador)
    {
        $this->Operador = $Operador;

        return $this;
    }

    /**
     * Get the value of Book_Day
     */ 
    public function getBook_Day()
    {
        return $this->Book_Day;
    }

    /**
     * Set the value of Book_Day
     *
     * @return  self
     */ 
    public function setBook_Day($Book_Day)
    {
        $this->Book_Day = $Book_Day;

        return $this;
    }

    /**
     * Get the value of Fecha_Entrada
     */ 
    public function getFecha_Entrada()
    {
        return $this->Fecha_Entrada;
    }

    /**
     * Set the value of Fecha_Entrada
     *
     * @return  self
     */ 
    public function setFecha_Entrada($Fecha_Entrada)
    {
        $this->Fecha_Entrada = $Fecha_Entrada;

        return $this;
    }

    /**
     * Get the value of Fecha_Salida
     */ 
    public function getFecha_Salida()
    {
        return $this->Fecha_Salida;
    }

    /**
     * Set the value of Fecha_Salida
     *
     * @return  self
     */ 
    public function setFecha_Salida($Fecha_Salida)
    {
        $this->Fecha_Salida = $Fecha_Salida;

        return $this;
    }

    /**
     * Get the value of Pension
     */ 
    public function getPension()
    {
        return $this->Pension;
    }

    /**
     * Set the value of Pension
     *
     * @return  self
     */ 
    public function setPension($Pension)
    {
        $this->Pension = $Pension;

        return $this;
    }

    /**
     * Get the value of Nombre
     */ 
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set the value of Nombre
     *
     * @return  self
     */ 
    public function setNombre($Nombre)
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    /**
     * Get the value of Apellidos
     */ 
    public function getApellidos()
    {
        return $this->Apellidos;
    }

    /**
     * Set the value of Apellidos
     *
     * @return  self
     */ 
    public function setApellidos($Apellidos)
    {
        $this->Apellidos = $Apellidos;

        return $this;
    }

    /**
     * Get the value of Numero_Adultos
     */ 
    public function getNumero_Adultos()
    {
        return $this->Numero_Adultos;
    }

    /**
     * Set the value of Numero_Adultos
     *
     * @return  self
     */ 
    public function setNumero_Adultos($Numero_Adultos)
    {
        $this->Numero_Adultos = $Numero_Adultos;

        return $this;
    }

    /**
     * Get the value of Numero_Ninios
     */ 
    public function getNumero_Ninios()
    {
        return $this->Numero_Ninios;
    }

    /**
     * Set the value of Numero_Ninios
     *
     * @return  self
     */ 
    public function setNumero_Ninios($Numero_Ninios)
    {
        $this->Numero_Ninios = $Numero_Ninios;

        return $this;
    }

    /**
     * Get the value of Pais
     */ 
    public function getPais()
    {
        return $this->Pais;
    }

    /**
     * Set the value of Pais
     *
     * @return  self
     */ 
    public function setPais($Pais)
    {
        $this->Pais = $Pais;

        return $this;
    }

    /**
     * Get the value of Mascota
     */ 
    public function getMascota()
    {
        return $this->Mascota;
    }

    /**
     * Set the value of Mascota
     *
     * @return  self
     */ 
    public function setMascota($Mascota)
    {
        $this->Mascota = $Mascota;

        return $this;
    }

    /**
     * Get the value of Prepago
     */ 
    public function getPrepago()
    {
        return $this->Prepago;
    }

    /**
     * Set the value of Prepago
     *
     * @return  self
     */ 
    public function setPrepago($Prepago)
    {
        $this->Prepago = $Prepago;

        return $this;
    }

    /**
     * Get the value of Precio_Total
     */ 
    public function getPrecio_Total()
    {
        return $this->Precio_Total;
    }

    /**
     * Set the value of Precio_Total
     *
     * @return  self
     */ 
    public function setPrecio_Total($Precio_Total)
    {
        $this->Precio_Total = $Precio_Total;

        return $this;
    }

    /**
     * Get the value of Comentario
     */ 
    public function getComentario()
    {
        return $this->Comentario;
    }

    /**
     * Set the value of Comentario
     *
     * @return  self
     */ 
    public function setComentario($Comentario)
    {
        $this->Comentario = $Comentario;

        return $this;
    }

    /**
     * Get the value of Estado
     */ 
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Set the value of Estado
     *
     * @return  self
     */ 
    public function setEstado($Estado)
    {
        $this->Estado = $Estado;

        return $this;
    }

    /**
     * Get the value of Numero_Apartamento
     */ 
    public function getNumero_Apartamento()
    {
        return $this->Numero_Apartamento;
    }

    /**
     * Set the value of Numero_Apartamento
     *
     * @return  self
     */ 
    public function setNumero_Apartamento($Numero_Apartamento)
    {
        $this->Numero_Apartamento = $Numero_Apartamento;

        return $this;
    }

    /**
     * Get the value of Telefono
     */ 
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set the value of Telefono
     *
     * @return  self
     */ 
    public function setTelefono($Telefono)
    {
        $this->Telefono = $Telefono;

        return $this;
    }

    /**
     * Get the value of Usuario_Creado
     */ 
    public function getUsuario_Creado()
    {
        return $this->Usuario_Creado;
    }

    /**
     * Set the value of Usuario_Creado
     *
     * @return  self
     */ 
    public function setUsuario_Creado($Usuario_Creado)
    {
        $this->Usuario_Creado = $Usuario_Creado;

        return $this;
    }

    /**
     * Get the value of Usuario_Modifica
     */ 
    public function getUsuario_Modifica()
    {
        return $this->Usuario_Modifica;
    }

    /**
     * Set the value of Usuario_Modifica
     *
     * @return  self
     */ 
    public function setUsuario_Modifica($Usuario_Modifica)
    {
        $this->Usuario_Modifica = $Usuario_Modifica;

        return $this;
    }

    /**
     * Get the value of Nombre_Archivo
     */ 
    public function getNombre_Archivo()
    {
        return $this->Nombre_Archivo;
    }

    /**
     * Set the value of Nombre_Archivo
     *
     * @return  self
     */ 
    public function setNombre_Archivo($Nombre_Archivo)
    {
        $this->Nombre_Archivo = $Nombre_Archivo;

        return $this;
    }

    /**
     * Get the value of Fecha_Creado
     */ 
    public function getFecha_Creado()
    {
        return $this->Fecha_Creado;
    }

    /**
     * Set the value of Fecha_Creado
     *
     * @return  self
     */ 
    public function setFecha_Creado($Fecha_Creado)
    {
        $this->Fecha_Creado = $Fecha_Creado;

        return $this;
    }
}
