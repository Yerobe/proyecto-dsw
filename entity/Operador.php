<?php

require_once "database/IEntity.php";

class Operador implements IEntity  // Operador de SGBD
{

    private $id;
    private $Nombre;
    private $Email;
    private $Url;
    private $Telefono;
    private $Estado;



    public function __construct($id = 0, $Nombre = " ", $Email = " ", $Url = " " , $Telefono = " ", $Estado = 0)
    {

        $this->id = $id;

        $this->nombre = $Nombre;

        $this->email = $Email;

        $this->url = $Url;

        $this->telefono = $Telefono;

        $this->estado = $Estado;
    }


    public function toArray(): array

    {

        return [

            "id" => $this->getId(),

            "Nombre" => $this->getNombre(),

            "Email" => $this->getEmail(),

            "Url" => $this->getUrl(),

            "Telefono" => $this->getTelefono(),

            "Estado" => $this->getEstado()

        ];
    }



   

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of url
     */ 
    public function getUrl()
    {
        return $this->Url;
    }

    /**
     * Set the value of url
     *
     * @return  self
     */ 
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get the value of telefono
     */ 
    public function getTelefono()
    {
        return $this->Telefono;
    }

    /**
     * Set the value of telefono
     *
     * @return  self
     */ 
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
}
