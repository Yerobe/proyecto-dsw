<?php

require_once "database/IEntity.php";

class Usuario implements IEntity // Usuario de SGBD
{

    private $NIF;
    private $Nombre;
    private $Apellidos;
    private $Permisos;
    private $Estado;
    private $Password;



    public function __construct($NIF = NULL, $Nombre = "Usuario_Nuevo", $Apellidos = "Apellidos_Nuevos", $Permisos = 4 , $Estado = 0, $Password = NULL)
    {

        $this->NIF = $NIF;

        $this->Nombre = $Nombre;

        $this->Apellidos = $Apellidos;

        $this->Permisos = $Permisos;

        $this->Estado = $Estado;

        $this->Password = $Password;
    }


    public function toArray(): array

    {

        return [

            "NIF" => $this->getNif(),

            "Nombre" => $this->getNombre(),

            "Apellidos" => $this->getApellidos(),

            "Permisos" => $this->getPermisos(),

            "Estado" => $this->getEstado(),

            "Password" => $this->getPassword()

        ];
    }



    /**
     * Get the value of nif
     */ 
    public function getNif()
    {
        return $this->NIF;
    }

    /**
     * Set the value of nif
     *
     * @return  self
     */ 
    public function setNif($nif)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->Nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of apellidos
     */ 
    public function getApellidos()
    {
        return $this->Apellidos;
    }

    /**
     * Set the value of apellidos
     *
     * @return  self
     */ 
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get the value of permisos
     */ 
    public function getPermisos()
    {
        return $this->Permisos;
    }

    /**
     * Set the value of permisos
     *
     * @return  self
     */ 
    public function setPermisos($permisos)
    {
        $this->permisos = $permisos;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->Estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
}
